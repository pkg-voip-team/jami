<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="uk" sourcelanguage="en">
<context>
    <name>CallAdapter</name>
    <message>
        <location filename="../src/app/calladapter.cpp" line="220"/>
        <source>Missed call</source>
        <translation>Пропущений виклик</translation>
    </message>
    <message>
        <location filename="../src/app/calladapter.cpp" line="221"/>
        <source>Missed call with %1</source>
        <translation>Пропущений виклик від %1</translation>
    </message>
    <message>
        <location filename="../src/app/calladapter.cpp" line="542"/>
        <source>Incoming call</source>
        <translation>Вхідний виклик</translation>
    </message>
    <message>
        <location filename="../src/app/calladapter.cpp" line="543"/>
        <source>%1 is calling you</source>
        <translation>%1 викликає вас</translation>
    </message>
    <message>
        <location filename="../src/app/calladapter.cpp" line="554"/>
        <source>is calling you</source>
        <translation>викликає вас</translation>
    </message>
    <message>
        <location filename="../src/app/calladapter.cpp" line="1052"/>
        <source>Screenshot</source>
        <translation>Скриншот</translation>
    </message>
</context>
<context>
    <name>ConversationsAdapter</name>
    <message>
        <location filename="../src/app/conversationsadapter.cpp" line="189"/>
        <source>%1 received a new message</source>
        <translation>% 1 отримав нову повідомлення</translation>
    </message>
    <message>
        <location filename="../src/app/conversationsadapter.cpp" line="244"/>
        <source>%1 received a new trust request</source>
        <translation>%1 отримав нову запит на довіру</translation>
    </message>
</context>
<context>
    <name>CurrentCall</name>
    <message>
        <location filename="../src/app/currentcall.cpp" line="185"/>
        <source>Me</source>
        <translation>Я</translation>
    </message>
</context>
<context>
    <name>CurrentConversation</name>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="137"/>
        <source>Private</source>
        <translation>Приватне</translation>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="139"/>
        <source>Private group (restricted invites)</source>
        <translation>Частна група (запрошення обмежені)</translation>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="141"/>
        <source>Private group</source>
        <translation>Приватна група</translation>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="143"/>
        <source>Public group</source>
        <translation>Публічна група</translation>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="324"/>
        <source>An error occurred while fetching this repository</source>
        <translation>Під час отримання цього репозиторію сталася помилка</translation>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="326"/>
        <source>Unrecognized conversation mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="330"/>
        <source>Not authorized to update conversation information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="332"/>
        <source>An error occurred while committing a new message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="328"/>
        <source>An invalid message was detected</source>
        <translation>Виявлено недійсне повідомлення</translation>
    </message>
</context>
<context>
    <name>JamiStrings</name>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="29"/>
        <source>Accept</source>
        <translation>Прийняти</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="30"/>
        <source>Accept in audio</source>
        <translation>Прийміть аудіо</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="31"/>
        <source>Accept in video</source>
        <translation>Прийміть в відео</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="32"/>
        <source>Refuse</source>
        <translation>Скидання</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="33"/>
        <source>End call</source>
        <translation>Закінчити дзвінок</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="34"/>
        <source>Incoming audio call from {}</source>
        <translation>Вхідний аудіо виклик від {}</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="35"/>
        <source>Incoming video call from {}</source>
        <translation>Вхідний відео виклик від {}</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="38"/>
        <source>Invitations</source>
        <translation>Запрошення</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="39"/>
        <source>Jami is a universal communication platform, with privacy as its foundation, that relies on a free distributed network for everyone.</source>
        <translation>Jami - це універсальна платформа комунікації, яка базується на приватності, яка залежить від безкоштовної розподіленої мережі для всіх.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="40"/>
        <source>Migrating to the Swarm technology will enable synchronizing this conversation across multiple devices and improve reliability. The legacy conversation history will be cleared in the process.</source>
        <translation>Перехід до технології Swarm дозволить синхронізувати цю розмову на декількох пристроях і поліпшити надійність.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="44"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="733"/>
        <source>Could not re-connect to the Jami daemon (jamid).
Jami will now quit.</source>
        <translation>Не змогла знову зв&apos;язатися з д&apos;ямоном Джамі.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="45"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="732"/>
        <source>Trying to reconnect to the Jami daemon (jamid)…</source>
        <translation>Спробую знову зв&apos;язатися з д&apos;ямітом (джамідом)...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="48"/>
        <source>Version</source>
        <translation>Версія</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="51"/>
        <source>Jami is a free universal communication software that respects the freedom and privacy of its users.</source>
        <translation>Jami - це безкоштовне універсальне програмне забезпечення для комунікації, яке поважає свободу і конфіденційність своїх користувачів.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="54"/>
        <source>Display QR code</source>
        <translation>Відобразити QR код</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="55"/>
        <source>Open settings</source>
        <translation>Відкрити налаштування</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="56"/>
        <source>Close settings</source>
        <translation>Закрити налаштування</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="57"/>
        <source>Add Account</source>
        <translation>Додати обліковий запис</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="60"/>
        <source>Add to conference</source>
        <translation>Додати до конференції</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="61"/>
        <source>Add to conversation</source>
        <translation>Додати до розмови</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="62"/>
        <source>Transfer this call</source>
        <translation>Передати цей виклик</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="63"/>
        <source>Transfer to</source>
        <translation>Передача до</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="66"/>
        <source>Authentication required</source>
        <translation>Потрібна аутентіфікация</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="67"/>
        <source>Your session has expired or been revoked on this device. Please enter your password.</source>
        <translation>Ваша сесія закінчилася або була скасована на цьому пристрої.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="68"/>
        <source>JAMS server</source>
        <translation>Сервер JAMS</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="69"/>
        <source>Authenticate</source>
        <translation>Оцінку</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="70"/>
        <source>Delete account</source>
        <translation>Видалити обліковий запис</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="71"/>
        <source>In progress…</source>
        <translation>В процесі...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="72"/>
        <source>Authentication failed</source>
        <translation>Помилка аутентіфікації</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="73"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="74"/>
        <source>Username</source>
        <translation>Ім’я</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="75"/>
        <source>Alias</source>
        <translation>Додаткове ім’я</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="78"/>
        <source>Allow incoming calls from unknown contacts</source>
        <translation>Дозвольте прийом покликів з невідомих контактів</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="79"/>
        <source>Convert your account into a rendezvous point</source>
        <translation>Переверніть свій рахунок в пункт зустрічі</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="80"/>
        <source>Automatically answer calls</source>
        <translation>Приймати дзвінки автоматично</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="81"/>
        <source>Enable custom ringtone</source>
        <translation>Увімкнути користувальницький рінгтон</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="82"/>
        <source>Select custom ringtone</source>
        <translation>Вибрати користувальницький рінгтон</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="83"/>
        <source>Select a new ringtone</source>
        <translation>Вибрати новий рінгтон</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="84"/>
        <source>Certificate File (*.crt)</source>
        <translation>Файл сертифікату (*.crt)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="85"/>
        <source>Audio File (*.wav *.ogg *.opus *.mp3 *.aiff *.wma)</source>
        <translation>Звуковий файл (*.wav *.ogg *.opus *.mp3 *.aiff *.wma)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="86"/>
        <source>Push-to-talk</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="87"/>
        <source>Enable push-to-talk</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="88"/>
        <source>Keyboard shortcut</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="89"/>
        <source>Change keyboard shortcut</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="92"/>
        <source>Change shortcut</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="93"/>
        <source>Press the key to be assigned to push-to-talk shortcut</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="94"/>
        <source>Assign</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="97"/>
        <source>Enable read receipts</source>
        <translation>Отримати квитанції з прочитання</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="98"/>
        <source>Send and receive receipts indicating that a message have been displayed</source>
        <translation>Відправляйте і отримуйте квитанції, що вказують, що повідомлення відображено</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="101"/>
        <source>Voicemail</source>
        <translation>Голосова пошта</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="102"/>
        <source>Voicemail dial code</source>
        <translation>Код виклику голосової пошти</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="105"/>
        <source>Security</source>
        <translation>Безпека</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="106"/>
        <source>Enable SDES key exchange</source>
        <translation>Обов&apos;язково обміну SDES-ключами</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="107"/>
        <source>Encrypt negotiation (TLS)</source>
        <translation>Шифрування переговорів (TLS)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="108"/>
        <source>CA certificate</source>
        <translation>CA сертифікат</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="109"/>
        <source>User certificate</source>
        <translation>Сертифікат користувача</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="110"/>
        <source>Private key</source>
        <translation>Приватний ключ</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="111"/>
        <source>Private key password</source>
        <translation>Пароль від приватного ключа</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="112"/>
        <source>Verify certificates for incoming TLS connections</source>
        <translation>Перевірка сертифікатів для вхідних підключень TLS</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="113"/>
        <source>Verify server TLS certificates</source>
        <translation>Перевірка сервісних TLS сертифікат </translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="114"/>
        <source>Require certificate for incoming TLS connections</source>
        <translation>Запрошуйте сертифікат для вхідних з&apos;єднань TLS</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="116"/>
        <source>Select a private key</source>
        <translation>Вибрати приватний ключ</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="117"/>
        <source>Select a user certificate</source>
        <translation>Вибрати сертифікат користувача</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="118"/>
        <source>Select a CA certificate</source>
        <translation>Виберіть сертифікат CA</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="120"/>
        <source>Key File (*.key)</source>
        <translation>Файл ключа (*.key)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="123"/>
        <source>Connectivity</source>
        <translation>З&apos;єднання</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="124"/>
        <source>Auto Registration After Expired</source>
        <translation>Автозапис після закінчення терміну</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="125"/>
        <source>Registration expiration time (seconds)</source>
        <translation>Час закінчення реєстрації (секунди)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="126"/>
        <source>Network interface</source>
        <translation>Мережевий інтерфейс</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="127"/>
        <source>Use UPnP</source>
        <translation>Використовувати UPnP</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="128"/>
        <source>Use TURN</source>
        <translation>Використовувати TURN</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="129"/>
        <source>TURN address</source>
        <translation>Адреса TURN</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="130"/>
        <source>TURN username</source>
        <translation>Обрати ім&apos;я</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="131"/>
        <source>TURN password</source>
        <translation>Обрати пароль</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="132"/>
        <source>TURN Realm</source>
        <translation>СВІДНА СІЛЬЯ</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="133"/>
        <source>Use STUN</source>
        <translation>Використовувати STUN</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="134"/>
        <source>STUN address</source>
        <translation>Адреса STUN</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="137"/>
        <source>Allow IP Auto Rewrite</source>
        <translation>Дозвольте автоматичне переписання IP</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="138"/>
        <source>Public address</source>
        <translation>Публічна адреса</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="139"/>
        <source>Use custom address and port</source>
        <translation>Використовувати нестандартні адресу та порт</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="140"/>
        <source>Address</source>
        <translation>Адреса</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="141"/>
        <source>Port</source>
        <translation>Порт</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="144"/>
        <source>Media</source>
        <translation>Медіа</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="145"/>
        <source>Enable video</source>
        <translation>Увімкнути відео</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="153"/>
        <source>SDP Session Negotiation (ICE Fallback)</source>
        <translation>Погодження сесій SDP (резервування ICE)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="154"/>
        <source>Only used during negotiation in case ICE is not supported</source>
        <translation>Використовується лише під час переговорів, якщо ICE не підтримується</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="155"/>
        <source>Audio RTP minimum Port</source>
        <translation>Мінімальний порт аудіо RTP</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="156"/>
        <source>Audio RTP maximum Port</source>
        <translation>Максимальний RTP аудіо Порт</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="157"/>
        <source>Video RTP minimum Port</source>
        <translation>Минімальний порт RTP</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="158"/>
        <source>Video RTP maximum port</source>
        <translation>Максимальний порт RTP відео</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="161"/>
        <source>Enable local peer discovery</source>
        <translation>Отримати локальне відкриття однолітків</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="162"/>
        <source>Connect to other DHT nodes advertising on your local network.</source>
        <translation>Підключайтеся до інших вузлів DHT, які рекламирують в вашій локальній мережі.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="164"/>
        <source>Enable proxy</source>
        <translation>Використовувати проксі</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="165"/>
        <source>Proxy address</source>
        <translation>Адреса проксі</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="166"/>
        <source>Bootstrap</source>
        <translation>Початкове завантаження</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="169"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="170"/>
        <source>Account</source>
        <translation>Обліковий запис</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="171"/>
        <source>General</source>
        <translation>Головні</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="172"/>
        <source>Extensions</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="182"/>
        <source>Audio</source>
        <translation>Звук</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="183"/>
        <source>Microphone</source>
        <translation>Мікрофон</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="184"/>
        <source>Select audio input device</source>
        <translation>Вибрати пристрій вхідного звуку</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="185"/>
        <source>Output device</source>
        <translation>Пристрій виведення</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="186"/>
        <source>Select audio output device</source>
        <translation>Вибрати пристрій виведення звуку</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="187"/>
        <source>Ringtone device</source>
        <translation>Звуково-звукований пристрій</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="188"/>
        <source>Select ringtone output device</source>
        <translation>Виберіть випускний пристрій званку</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="189"/>
        <source>Audio manager</source>
        <translation>Менеджер аудіо</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="193"/>
        <source>Video</source>
        <translation>Відео</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="194"/>
        <source>Select video device</source>
        <translation>Вибрати пристрій відео</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="195"/>
        <source>Device</source>
        <translation>Пристрій</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="196"/>
        <source>Resolution</source>
        <translation>Роздільна здатність</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="197"/>
        <source>Select video resolution</source>
        <translation>Вибрати роздільність відео</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="198"/>
        <source>Frames per second</source>
        <translation>Кадрів за секунду</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="199"/>
        <source>Select video frame rate (frames per second)</source>
        <translation>Виберіть швидкість кадрів відео (кадри на секунду)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="200"/>
        <source>Enable hardware acceleration</source>
        <translation>Обов&apos;язково прискорення обладнання</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="203"/>
        <source>Select screen sharing frame rate (frames per second)</source>
        <translation>Виберіть частоту кадрів для розділу екрану (кадри на секунду)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="204"/>
        <source>no video</source>
        <translation>немає відео</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="209"/>
        <source>Back up account here</source>
        <translation>Задосконалюйте рахунок тут</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="210"/>
        <source>Back up account</source>
        <translation>Резервна копія облікового запису</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="284"/>
        <source>Unavailable</source>
        <translation>Недоступна</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="311"/>
        <source>Turn off sharing</source>
        <translation>Знімайте ділянку</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="312"/>
        <source>Stop location sharing in this conversation (%1)</source>
        <translation>Перестань ділитися місцем в цій розмові (% 1)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="335"/>
        <source>Hide chat</source>
        <translation>Сховати розмову</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="344"/>
        <source>Back to Call</source>
        <translation>Повертаюся до телефону</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="353"/>
        <source>Scroll to end of conversation</source>
        <translation>Перевертайте до кінця розмови</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="371"/>
        <source>You can choose a username to help others more easily find and reach you on Jami.</source>
        <translation>Ви можете обрати імʼя користувача, щоб допомогти іншим легше знайти і звʼязатися з Вами у Jami.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="378"/>
        <source>Are you sure you would like to join Jami without a username?
If yes, only a randomly generated 40-character identifier will be assigned to this account.</source>
        <translation>Якщо так, то на цей рахунок буде присвоєно лише випадково генерований 40-знаковий ідентифікатор.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="379"/>
        <source>- 32 characters maximum
- Alphabetical characters (A to Z and a to z)
- Numeric characters (0 to 9)
- Special characters allowed: dash (-)</source>
        <translation>- Максимально 32 символи - Алфавітні символи (A до Z і a до z) - Цифрові символи (0 до 9) - Спеціальні символи дозволені: штрих (-)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="385"/>
        <source>Your account will be created and stored locally.</source>
        <translation>Ваш рахунок буде створений і зберігається місцево.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="386"/>
        <source>Choosing a username is recommended, and a chosen username CANNOT be changed later.</source>
        <translation>Вибирати ім&apos;я користувача рекомендується, а вибране ім&apos;я користувача НЕ МОЖЕ бути змінено пізніше.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="387"/>
        <source>Encrypting your account with a password is optional, and if the password is lost it CANNOT be recovered later.</source>
        <translation>Зашифрування вашого рахунку паролем є не обов&apos;язковим, і якщо пароль зникне, його НЕ МОЖЕ відновити пізніше.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="388"/>
        <source>Setting a profile picture and nickname is optional, and can also be changed later in the settings.</source>
        <translation>Установка зображення профілю і прізвища є не обов&apos;язковим, і також можна змінити пізніше в налаштуваннях.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="397"/>
        <source>TLS</source>
        <translation>TLS</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="398"/>
        <source>UDP</source>
        <translation>УПД</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="399"/>
        <source>Display Name</source>
        <translation>Відображаєме Ім’я</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="403"/>
        <source>A chosen username can help to be found more easily on Jami.
If a username is not chosen, a randomly generated 40-character identifier will be assigned to this account as a username. It is more difficult to be found and reached with this identifier.</source>
        <translation>Вибрано ім&apos;я користувача може допомогти знайти його більш легко на Jami. Якщо ім&apos;я користувача не вибрано, випадково генерований 40-знаковий ідентифікатор буде присвоений цьому рахунку як ім&apos;я користувача.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="406"/>
        <source>This Jami account exists only on this device.
The account will be lost if this device is lost or the application is uninstalled. It is recommended to make a backup of this account.</source>
        <translation>Цей акаунт Jami існує тільки на цьому пристрої. Аккаунт буде втрачений, якщо цей апарат зникне або додаток буде відстановлений. Рекомендується зробити резервне копию цього акаунту.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="421"/>
        <source>Encrypt account</source>
        <translation>Зашифрований рахунок</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="424"/>
        <source>Back up account to a .gz file</source>
        <translation>Записати резервну копію облікового запису у файл .gz</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="430"/>
        <source>This profile is only shared with this account's contacts.
The profile can be changed at all times from the account&apos;s settings.</source>
        <translation>Цей профіль ділиться лише з контактами цього рахунку. Профіль можна змінювати в будь-який час з налаштувань рахунку.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="431"/>
        <source>Encrypt account with a password</source>
        <translation>Зашифруйте обліковий запис паролем</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="432"/>
        <source>A Jami account is created and stored locally only on this device, as an archive containing your account keys. Access to this archive can optionally be protected by a password.</source>
        <translation>Аккаунт Jami створюється і зберігається локально тільки на цьому пристрої, як архів, що містить ключі до вашого облікового запису.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="433"/>
        <source>Please note that if you lose your password, it CANNOT be recovered!</source>
        <translation>Будь ласка, зверніть увагу, що якщо ви втратите пароль, його НЕ МОЖЕ відновити!</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="459"/>
        <source>Would you really like to delete this account?</source>
        <translation>Дійсно бажаєте видалити цей обліковий запис?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="460"/>
        <source>If your account has not been backed up or added to another device, your account and registered username will be IRREVOCABLY LOST.</source>
        <translation>Якщо ваш обліковий запис не був заставлений на резервне запис або доданий до іншого пристрою, ваш обліковий запис і зареєстроване ім&apos;я користувача будуть незворотно втрачені.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="471"/>
        <source>Dark</source>
        <translation>Темна</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="472"/>
        <source>Light</source>
        <translation>Світла</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="480"/>
        <source>Include local video in recording</source>
        <translation>Включіть місцеве відео в запис</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="481"/>
        <source>Default settings</source>
        <translation>Позадумовні налаштування</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="484"/>
        <source>Enable typing indicators</source>
        <translation>Отримати показники типування</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="485"/>
        <source>Send and receive typing indicators showing that a message is being typed.</source>
        <translation>Відправляйте і отримуйте показники для впису, що повідомлення вписується.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="486"/>
        <source>Show link preview in conversations</source>
        <translation>Покажіть перегляд посилань у розмовах</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="508"/>
        <source>Delete file from device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="525"/>
        <source>Content access error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="526"/>
        <source>Content not found</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="589"/>
        <source>Enter account password</source>
        <translation>Введіть пароль рахунку</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="590"/>
        <source>This account is password encrypted, enter the password to generate a PIN code.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="592"/>
        <source>PIN expired</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="593"/>
        <source>On another device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="594"/>
        <source>Install and launch Jami, select &quot;Import from another device&quot; and scan the QR code.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="595"/>
        <source>Link new device</source>
        <translation>Зв&apos;язок нового пристрою</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="596"/>
        <source>In Jami, scan QR code or manually enter the PIN.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="597"/>
        <source>The PIN code is valid for: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="600"/>
        <source>Enter password</source>
        <translation>Ввести пароль</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="602"/>
        <source>Enter account password to confirm the removal of this device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="742"/>
        <source>Show less</source>
        <translation>Покажіть менше</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="744"/>
        <source>Show preview</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="745"/>
        <source>Continue editing</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="748"/>
        <source>Strikethrough</source>
        <translation>Закреслений</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="754"/>
        <source>Unordered list</source>
        <translation>Невпорядкований список</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="755"/>
        <source>Ordered list</source>
        <translation>Упорядкований список</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="758"/>
        <source>Press Shift+Enter to insert a new line</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="759"/>
        <source>Press Enter to insert a new line</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="772"/>
        <source>Select dedicated device for hosting future calls in this swarm. If not set, the host will be the device starting a call.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="773"/>
        <source>Select this device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="774"/>
        <source>Select device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="833"/>
        <source>Appearance</source>
        <translation>Вигляд</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="852"/>
        <source>Free and private sharing. &lt;a href=&quot;https://jami.net/donate/&quot;&gt;Donate&lt;/a&gt; to expand it.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="853"/>
        <source>Donate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="854"/>
        <source>If you enjoy using Jami and believe in our mission, would you make a donation?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="855"/>
        <source>Not now</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="856"/>
        <source>Enable donation campaign</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="859"/>
        <source>Enter</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="860"/>
        <source>Shift+Enter</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="861"/>
        <source>Enter or Shift+Enter to insert a new line</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="875"/>
        <source>View</source>
        <translation>Вигляд</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="862"/>
        <source>Text formatting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="865"/>
        <source>Connected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="866"/>
        <source>Connecting TLS</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="867"/>
        <source>Connecting ICE</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="868"/>
        <source>Connecting</source>
        <translation>З&apos;єднання</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="869"/>
        <source>Waiting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="870"/>
        <source>Contact</source>
        <translation>Контакт</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="871"/>
        <source>Connection</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="872"/>
        <source>Channels</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="873"/>
        <source>Copy all data</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="874"/>
        <source>Remote: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="496"/>
        <source>Accept transfer limit (in Mb)</source>
        <translation>Прийміть ліміт переказу (в МБ)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="516"/>
        <source>A new version of Jami was found
Would you like to update now?</source>
        <translation>У нас знайдена нова версія Jami. Хочете оновлювати її?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="539"/>
        <source>Save recordings to</source>
        <translation>Заховувати записи до</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="540"/>
        <source>Save screenshots to</source>
        <translation>Зберігайте скриншоти до</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="582"/>
        <source>Select &quot;Link another device&quot;</source>
        <translation>Виберіть &quot;Звязати інше пристрої&quot;</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="609"/>
        <source>Choose a picture as your avatar</source>
        <translation>Виберіть фотографію як свій аватар</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="685"/>
        <source>Share freely and privately with Jami</source>
        <translation>Поділяйтеся з Джамі вільно і приватно</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="711"/>
        <source>Unban</source>
        <translation>Увільнення</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="728"/>
        <source>Add</source>
        <translation>Додати</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="737"/>
        <source>more emojis</source>
        <translation>більше емодіз</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="761"/>
        <source>Reply to</source>
        <translation>Відповідь на</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="762"/>
        <source>In reply to</source>
        <translation>Відповідно до</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="763"/>
        <source> replied to</source>
        <translation>відповів на</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="765"/>
        <source>Reply</source>
        <translation>Відповісти</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="464"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="767"/>
        <source>Edit</source>
        <translation>Редагувати</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="768"/>
        <source>Edited</source>
        <translation>Редагування</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="769"/>
        <source>Join call</source>
        <translation>Приєднати виклик</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="770"/>
        <source>A call is in progress. Do you want to join the call?</source>
        <translation>Зовсім не так.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="771"/>
        <source>Current host for this swarm seems unreachable. Do you want to host the call?</source>
        <translation>Теперішній хост для цього рога здається недосяжним.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="775"/>
        <source>Remove current device</source>
        <translation>Виведіть поточне пристрої</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="776"/>
        <source>Host only this call</source>
        <translation>Провести лише цей дзвінк</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="777"/>
        <source>Host this call</source>
        <translation>Прийміть цей дзвінк</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="778"/>
        <source>Make me the default host for future calls</source>
        <translation>Зробіть мене замовчуванням хост для майбутніх дзвінків</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="793"/>
        <source>Mute conversation</source>
        <translation>Немовний розмову</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="796"/>
        <source>Default host (calls)</source>
        <translation>Позавтовий хост (званки)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="799"/>
        <source>None</source>
        <translation>Нічого</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="813"/>
        <source>Tip</source>
        <translation>Порада</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="815"/>
        <source>Add a profile picture and nickname to complete your profile</source>
        <translation>Додайте картинку профілю та імʼя, щоб завершити свій профіль</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="36"/>
        <source>Start swarm</source>
        <translation>Почнеться рога</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="37"/>
        <source>Create swarm</source>
        <translation>Створюйте ров</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="176"/>
        <source>Call settings</source>
        <translation>Налаштування дзвінків</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="115"/>
        <source>Disable secure dialog check for incoming TLS data</source>
        <translation>Відключити безпечний перевірка діалогу для вхідних даних TLS</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="146"/>
        <source>Video codecs</source>
        <translation>Відео кодеки</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="147"/>
        <source>Audio codecs</source>
        <translation>Аудіо кодеки</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="150"/>
        <source>Name server</source>
        <translation>Сервер імен</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="163"/>
        <source>OpenDHT configuration</source>
        <translation>Налаштування OpenDHT</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="201"/>
        <source>Mirror local video</source>
        <translation>Зірне місцеве відео</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="207"/>
        <source>Why should I back-up this account?</source>
        <translation>Чому мені потрібно створити резервну копію цього облікового запису?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="211"/>
        <source>Success</source>
        <translation>Успішно</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="212"/>
        <source>Error</source>
        <translation>Помилка</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="213"/>
        <source>Jami archive files (*.gz)</source>
        <translation>Файли архіву Jami (*.gz)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="214"/>
        <source>All files (*)</source>
        <translation>Усі файли (*)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="217"/>
        <source>Reinstate as contact</source>
        <translation>Відновити контакт</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="218"/>
        <source>name</source>
        <translation>назва</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="219"/>
        <source>Identifier</source>
        <translation>Ідентифікатор</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="222"/>
        <source>is recording</source>
        <translation>записується</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="223"/>
        <source>are recording</source>
        <translation>записують</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="224"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="716"/>
        <source>Mute</source>
        <translation>Немов</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="225"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="717"/>
        <source>Unmute</source>
        <translation>Незамовлений</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="226"/>
        <source>Pause call</source>
        <translation>Призупинити дзвінок</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="227"/>
        <source>Resume call</source>
        <translation>Продовжити дзвінок</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="228"/>
        <source>Mute camera</source>
        <translation>Невідомі камери</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="229"/>
        <source>Unmute camera</source>
        <translation>Незамовлена камера</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="230"/>
        <source>Add participant</source>
        <translation>Додати учасників</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="231"/>
        <source>Add participants</source>
        <translation>Додавати учасників</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="232"/>
        <source>Details</source>
        <translation>Подробиці</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="177"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="233"/>
        <source>Chat</source>
        <translation>Чат</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="174"/>
        <source>Manage account</source>
        <translation>Керувати акаунтом</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="175"/>
        <source>Linked devices</source>
        <translation>Привʼязані пристрої</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="178"/>
        <source>Advanced settings</source>
        <translation>Розширені налаштування</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="179"/>
        <source>Audio and Video</source>
        <translation>Аудіо і відео</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="190"/>
        <source>Sound test</source>
        <translation>Звуковий тест</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="202"/>
        <source>Screen sharing</source>
        <translation>Розповсюдження екрану</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="208"/>
        <source>Your account only exists on this device. If you lose your device or uninstall the application, your account will be deleted and CANNOT be recovered. You can &lt;a href=&apos;blank&apos;&gt; back up your account &lt;/a&gt; now or later (in the Account Settings).</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="234"/>
        <source>More options</source>
        <translation>Більше параметрів</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="235"/>
        <source>Mosaic</source>
        <translation>Мозаїка</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="236"/>
        <source>Participant is still muted on their device</source>
        <translation>Учасники все ще не можуть прослуховувати на своєму пристрої</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="237"/>
        <source>You are still muted on your device</source>
        <translation>Ви все ще не вимовлені на своєму пристрої</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="238"/>
        <source>You are still muted by moderator</source>
        <translation>Ви все ще затишаєтеся від модератора</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="239"/>
        <source>You are muted by a moderator</source>
        <translation>Ви затушені модератором</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="240"/>
        <source>Moderator</source>
        <translation>Модератор</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="241"/>
        <source>Host</source>
        <translation>Гост</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="242"/>
        <source>Local and Moderator muted</source>
        <translation>Місцевий і Модератор затиснуті</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="243"/>
        <source>Moderator muted</source>
        <translation>Модератор затиснутий</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="244"/>
        <source>Not muted</source>
        <translation>Не затиснуто</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="245"/>
        <source>On the side</source>
        <translation>На боці</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="246"/>
        <source>On the top</source>
        <translation>На вершині</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="247"/>
        <source>Hide self</source>
        <translation>Сховати себе</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="248"/>
        <source>Hide spectators</source>
        <translation>Сховати глядачів</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="251"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="554"/>
        <source>Copy</source>
        <translation>Копіювати</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="252"/>
        <source>Share</source>
        <translation>Поділитися</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="253"/>
        <source>Cut</source>
        <translation>Вирізати</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="254"/>
        <source>Paste</source>
        <translation>Вставити</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="257"/>
        <source>Start video call</source>
        <translation>Розпочати відеодзвінок</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="258"/>
        <source>Start audio call</source>
        <translation>Розпочати аудіодзвінок</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="259"/>
        <source>Clear conversation</source>
        <translation>Очистити розмову</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="260"/>
        <source>Confirm action</source>
        <translation>Підтвердження дії</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="261"/>
        <source>Remove conversation</source>
        <translation>Вилучити розмову</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="262"/>
        <source>Would you really like to remove this conversation?</source>
        <translation>Дійсно бажаєте вилучити цю розмову</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="263"/>
        <source>Would you really like to block this conversation?</source>
        <translation>Дійсно бажаєте заблокувати цю розмову?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="264"/>
        <source>Remove contact</source>
        <translation>Видалити контакт</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="265"/>
        <source>Block contact</source>
        <translation>Заблокувати контакт</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="266"/>
        <source>Conversation details</source>
        <translation>Деталі розмови</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="267"/>
        <source>Contact details</source>
        <translation>Деталі контакту</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="270"/>
        <source>Sip input panel</source>
        <translation>Вхідний панель</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="271"/>
        <source>Transfer call</source>
        <translation>Перенесення виклику</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="272"/>
        <source>Stop recording</source>
        <translation>Зупинити запис</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="273"/>
        <source>Start recording</source>
        <translation>Розпочати запис</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="274"/>
        <source>View full screen</source>
        <translation>Показати на весь екран</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="275"/>
        <source>Share screen</source>
        <translation>Спільний перегляд екрана</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="276"/>
        <source>Share window</source>
        <translation>Показати вікно</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="277"/>
        <source>Stop sharing screen or file</source>
        <translation>Перестань ділитися екраном або файлом</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="278"/>
        <source>Share screen area</source>
        <translation>Поділіться екраном площа</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="279"/>
        <source>Share file</source>
        <translation>Відправити файл</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="280"/>
        <source>Select sharing method</source>
        <translation>Виберіть метод обміну</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="281"/>
        <source>View plugin</source>
        <translation>Переглянути втулку</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="282"/>
        <source>Advanced information</source>
        <translation>Передові інформація</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="283"/>
        <source>No video device</source>
        <translation>Без відеоприладу</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="285"/>
        <source>Lower hand</source>
        <translation>Нижня рука</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="286"/>
        <source>Raise hand</source>
        <translation>Підняти руку</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="287"/>
        <source>Layout settings</source>
        <translation>Настройки розкладу</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="288"/>
        <source>Take tile screenshot</source>
        <translation>Зробіть скриншот плитки</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="289"/>
        <source>Screenshot saved to %1</source>
        <translation>Стрима екрану збереглася до % 1</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="290"/>
        <source>File saved to %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="293"/>
        <source>Renderers information</source>
        <translation>Інформація про подачі</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="294"/>
        <source>Call information</source>
        <translation>Інформація про дзвінки</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="295"/>
        <source>Peer number</source>
        <translation>Номер однолітків</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="296"/>
        <source>Call id</source>
        <translation>Позвінок ID</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="297"/>
        <source>Sockets</source>
        <translation>Заводи</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="298"/>
        <source>Video codec</source>
        <translation>Віддільний кодек</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="299"/>
        <source>Hardware acceleration</source>
        <translation>Гардурова швидкість</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="300"/>
        <source>Video bitrate</source>
        <translation>Відріжка від відео</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="301"/>
        <source>Audio codec</source>
        <translation>Аудіокодек</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="302"/>
        <source>Renderer id</source>
        <translation>Ідентифікатор видавець</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="303"/>
        <source>Fps</source>
        <translation>Фпс</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="306"/>
        <source>Share location</source>
        <translation>Поділитися геоданними</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="307"/>
        <source>Stop sharing</source>
        <translation>Зупинити поширення</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="308"/>
        <source>Your precise location could not be determined.
In Device Settings, please turn on &quot;Location Services&quot;.
Other participants&apos; location can still be received.</source>
        <translation>Точне місцезнаходження не вдалося визначити. У налаштуваннях пристрою, будь ласка, включте &quot;Сервіси місцезнаходження&quot;.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="309"/>
        <source>Your precise location could not be determined. Please check your Internet connection.</source>
        <translation>Точне місцезнаходження не було встановлено.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="310"/>
        <source>Turn off location sharing</source>
        <translation>Заключити обмін місцевості</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="313"/>
        <source>Location is shared in several conversations</source>
        <translation>Місце об&apos;єднується в декількох розмовах</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="314"/>
        <source>Pin map to be able to share location or to turn off location in specific conversations</source>
        <translation>Карта PIN для обміну місцевістю або відключення місця в конкретних розмовах</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="315"/>
        <source>Location is shared in several conversations, click to choose how to turn off location sharing</source>
        <translation>Місце поділяється в декількох розмов, натисніть, щоб вибрати, як відключити поділу місця</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="316"/>
        <source>Share location to participants of this conversation (%1)</source>
        <translation>Поділиться місцезнаходженням учасників цієї розмови (%1)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="318"/>
        <source>Reduce</source>
        <translation>Зменшити</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="319"/>
        <source>Drag</source>
        <translation>Перетягнути</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="320"/>
        <source>Center</source>
        <translation>Центр</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="322"/>
        <source>Unpin</source>
        <translation>Відкриття</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="323"/>
        <source>Pin</source>
        <translation>Пінка</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="324"/>
        <source>Position share duration</source>
        <translation>Тривалість позиційних акцій</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="325"/>
        <source>Location sharing</source>
        <translation>Розділ місцезнаходження</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="326"/>
        <source>Unlimited</source>
        <translation>Не обмежено</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="327"/>
        <source>1 min</source>
        <translation>1 хвіліну</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="328"/>
        <source>%1h%2min</source>
        <translation>% 1h% 2min</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="329"/>
        <source>%1h</source>
        <translation>% 1 h</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="330"/>
        <source>%1min%2s</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="331"/>
        <source>%1min</source>
        <translation>%1 хвилину</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="332"/>
        <source>%sec</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="336"/>
        <source>Place audio call</source>
        <translation>Почати аудіо дзвінок</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="337"/>
        <source>Place video call</source>
        <translation>Почати відео дзвінок</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="338"/>
        <source>Show available plugins</source>
        <translation>Показати доступні втулки</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="339"/>
        <source>Add to conversations</source>
        <translation>Додати до розмови</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="340"/>
        <source>This is the error from the backend: %0</source>
        <translation>Це помилка від заднього кінця: %0</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="341"/>
        <source>The account is disabled</source>
        <translation>Аккаунт відключений</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="342"/>
        <source>No network connectivity</source>
        <translation>Відсутнє з’єднання з мережею</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="343"/>
        <source>Deleted message</source>
        <translation>Збито повідомлення</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="347"/>
        <source>Jump to</source>
        <translation>Скач до</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="348"/>
        <source>Messages</source>
        <translation>Повідомлення</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="349"/>
        <source>Files</source>
        <translation>Файли</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="350"/>
        <source>Search</source>
        <translation>Пошук</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="354"/>
        <source>{} is typing…</source>
        <translation>{} пише...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="355"/>
        <source>{} are typing…</source>
        <translation>{} типують...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="356"/>
        <source>Several people are typing…</source>
        <translation>Кілька людей пише...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="357"/>
        <source> and </source>
        <translation>і </translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="360"/>
        <source>Enter the Jami Account Management Server (JAMS) URL</source>
        <translation>Уведіть URL сервера керування обліковими записами Jami (JAMS)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="361"/>
        <source>Jami Account Management Server URL</source>
        <translation>URL cервера керування обліковими записами Jami</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="362"/>
        <source>Enter JAMS credentials</source>
        <translation>Введіть ідентифікацію JAMS</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="363"/>
        <source>Connect</source>
        <translation>З&apos;єднати</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="364"/>
        <source>Creating account…</source>
        <translation>Створення облікового запису…</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="367"/>
        <source>Choose name</source>
        <translation>Обрати назву</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="368"/>
        <source>Choose username</source>
        <translation>Обрати імʼя користувача</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="369"/>
        <source>Choose a username</source>
        <translation>Виберіть ім&apos;я користувача</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="428"/>
        <source>Encrypt account with password</source>
        <translation>Зашифрувати мій профіль за допомогою пароля</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="370"/>
        <source>Confirm password</source>
        <translation>Підтвердити пароль</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="372"/>
        <source>Choose a name for your rendezvous point</source>
        <translation>Виберіть ім&apos;я для місця зустрічі</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="373"/>
        <source>Choose a name</source>
        <translation>Виберіть ім&apos;я</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="374"/>
        <source>Invalid name</source>
        <translation>Недійсне ім&apos;я</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="375"/>
        <source>Invalid username</source>
        <translation>Недопустиме ім&apos;я користувача</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="376"/>
        <source>Name already taken</source>
        <translation>Ім&apos;я вже прийнято</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="377"/>
        <source>Username already taken</source>
        <translation>Ім&apos;я користувача вже зайнято</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="382"/>
        <source>Good to know</source>
        <translation>Добре знати.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="383"/>
        <source>Local</source>
        <translation>Місцеві</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="384"/>
        <source>Encrypt</source>
        <translation>Зашифровано</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="391"/>
        <source>SIP account</source>
        <translation>Спільний рахунок</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="392"/>
        <source>Proxy</source>
        <translation>Проксі</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="393"/>
        <source>Server</source>
        <translation>Сервер</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="394"/>
        <source>Configure an existing SIP account</source>
        <translation>Дозволити сповіщення</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="395"/>
        <source>Personalize account</source>
        <translation>Персоналізуйте обліковий запис</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="396"/>
        <source>Add SIP account</source>
        <translation>Додати обліковий запис SIP</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="402"/>
        <source>Your profile is only shared with your contacts.
Your picture and your nickname can be changed at all time in the settings of your account.</source>
        <translation>Ваш профіль ділиться лише з вашими контактами. Вашу фотографію і прізвище можна змінювати в будь-який час в налаштуваннях вашого облікового запису.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="404"/>
        <source>Your Jami account is registered only on this device as an archive containing the keys of your account. Access to this archive can be protected by a password.</source>
        <translation>Ваш акаунт Jami зареєстрований тільки на цьому пристрої як архів, що містить ключі вашого акаунту.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="405"/>
        <source>Backup account</source>
        <translation>Резервна копія облікового запису</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="407"/>
        <source>Delete your account</source>
        <translation>Збийте свій рахунок</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="408"/>
        <source>If your account has not been backed up or added to another device, your account and registered name will be irrevocably lost.</source>
        <translation>Якщо ваш обліковий запис не був заставлений на резервне запис або доданий до іншого пристрою, ваш обліковий запис і зареєстроване ім&apos;я будуть незворотно втрачені.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="409"/>
        <source>List of the devices that are linked to this account:</source>
        <translation>Перелік пристроїв, які пов&apos;язані з цим рахунком:</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="410"/>
        <source>This device</source>
        <translation>Цей пристрій</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="411"/>
        <source>Other linked devices</source>
        <translation>Інші привʼязані пристрої</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="414"/>
        <source>Backup successful</source>
        <translation>Захист успішні</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="415"/>
        <source>Backup failed</source>
        <translation>Захист не виконаний</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="416"/>
        <source>Password changed successfully</source>
        <translation>Пароль успішно змінилася</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="417"/>
        <source>Password change failed</source>
        <translation>Зміна пароля не вдалося</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="418"/>
        <source>Password set successfully</source>
        <translation>Параль задано успішно</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="419"/>
        <source>Password set failed</source>
        <translation>Не вдалося вказати пароль</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="420"/>
        <source>Change password</source>
        <translation>Змінити пароль</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="434"/>
        <source>Enter a nickname, surname…</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="435"/>
        <source>Use this account on other devices</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="436"/>
        <source>This account is created and stored locally, if you want to use it on another device you have to link the new device to this account.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="465"/>
        <source>Device name:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="550"/>
        <source>Markdown</source>
        <translation>Маркадоун</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="617"/>
        <source>Auto update</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="618"/>
        <source>Disable all</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="619"/>
        <source>Installed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="620"/>
        <source>Install</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="621"/>
        <source>Installing</source>
        <translation>Установка</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="622"/>
        <source>Install manually</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="623"/>
        <source>Install an extension directly from your device.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="624"/>
        <source>Available</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="625"/>
        <source>Plugins store is not available</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="627"/>
        <source>Installation failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="628"/>
        <source>The installation of the plugin failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="646"/>
        <source>Version %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="647"/>
        <source>Last update %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="648"/>
        <source>By %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="649"/>
        <source>Proposed by %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="652"/>
        <source>More information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="739"/>
        <source>Audio message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="740"/>
        <source>Video message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="741"/>
        <source>Show more</source>
        <translation>Покажіть більше</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="746"/>
        <source>Bold</source>
        <translation>Жирний</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="747"/>
        <source>Italic</source>
        <translation>Курсив</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="749"/>
        <source>Title</source>
        <translation>Заголовок</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="750"/>
        <source>Heading</source>
        <translation>Заголовок</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="751"/>
        <source>Link</source>
        <translation>Зв&apos;язати</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="752"/>
        <source>Code</source>
        <translation>Код</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="753"/>
        <source>Quote</source>
        <translation>Цитата</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="756"/>
        <source>Show formatting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="757"/>
        <source>Hide formatting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="824"/>
        <source>Share your Jami identifier in order to be contacted more easily!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="825"/>
        <source>Jami identity</source>
        <translation>Ідентифікація Джамі</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="826"/>
        <source>Show fingerprint</source>
        <translation>Покажіть відбитки пальців</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="827"/>
        <source>Show registered name</source>
        <translation>Покажіть зареєстроване ім&apos;я</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="830"/>
        <source>Enabling your account allows you to be contacted on Jami</source>
        <translation>Занягчи свій акаунт, ви зможете зв&apos;язатися з ним на Jami.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="836"/>
        <source>Experimental</source>
        <translation>Експериментальний</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="839"/>
        <source>Ringtone</source>
        <translation>Рингтон</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="842"/>
        <source>Rendezvous point</source>
        <translation>Точка зустрічі</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="845"/>
        <source>Moderation</source>
        <translation>Умірливість</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="848"/>
        <source>Theme</source>
        <translation>Тема</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="849"/>
        <source>Text zoom level</source>
        <translation>Рівень зуму тексту</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="422"/>
        <source>Set a password</source>
        <translation>Задати пароль</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="423"/>
        <source>Change current password</source>
        <translation>Змінить поточний пароль</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="425"/>
        <source>Display advanced settings</source>
        <translation>Показати розширені налаштування</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="426"/>
        <source>Hide advanced settings</source>
        <translation>Сховати розширені налаштування</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="173"/>
        <source>Enable account</source>
        <translation>Дозволити обліковий запис</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="427"/>
        <source>Advanced account settings</source>
        <translation>Розширені налаштування облікового запису</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="429"/>
        <source>Customize profile</source>
        <translation>Налаштувати профіль</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="440"/>
        <source>Set username</source>
        <translation>Наведіть ім&apos;я користувача</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="441"/>
        <source>Registering name</source>
        <translation>Назва реєстрації</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="444"/>
        <source>Identity</source>
        <translation>Особа</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="447"/>
        <source>Link a new device to this account</source>
        <translation>Звязати нове пристрої до цього обліку</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="448"/>
        <source>Exporting account…</source>
        <translation>Експортування облікового запису…</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="449"/>
        <source>Remove Device</source>
        <translation>Вилучити пристрій</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="450"/>
        <source>Are you sure you wish to remove this device?</source>
        <translation>Ви впевнені, що хочете видалити це пристрої?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="451"/>
        <source>Your PIN is:</source>
        <translation>Ваш PIN:</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="452"/>
        <source>Error connecting to the network.
Please try again later.</source>
        <translation>Помилка підключення до мережі. Пробуйте ще раз пізніше.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="455"/>
        <source>Banned</source>
        <translation>Заборонена</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="456"/>
        <source>Banned contacts</source>
        <translation>Заборонені контакти</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="467"/>
        <source>Device Id</source>
        <translation>ІД пристрою</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="470"/>
        <source>System</source>
        <translation>Система</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="473"/>
        <source>Select a folder</source>
        <translation>Вилучити теку</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="474"/>
        <source>Enable notifications</source>
        <translation>Дозволити сповіщення</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="478"/>
        <source>Launch at startup</source>
        <translation>Запускати під час запуску</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="479"/>
        <source>Choose download directory</source>
        <translation>Обрати каталог для завантажень</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="487"/>
        <source>Preview requires downloading content from third-party servers.</source>
        <translation>Вигляд вимагає завантаження контенту з серверів третіх осіб.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="489"/>
        <source>User interface language</source>
        <translation>Мова інтерфейсу</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="490"/>
        <source>Vertical view</source>
        <translation>Вертикальне подання</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="491"/>
        <source>Horizontal view</source>
        <translation>Горизонтальне подання</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="494"/>
        <source>File transfer</source>
        <translation>Передача файлів</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="495"/>
        <source>Automatically accept incoming files</source>
        <translation>Автоматично приймають вхідні файли</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="497"/>
        <source>in MB, 0 = unlimited</source>
        <translation>в MB, 0 = не обмежено</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="500"/>
        <source>Register</source>
        <translation>Зареєструватися</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="501"/>
        <source>Incorrect password</source>
        <translation>Неправильний пароль</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="502"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="519"/>
        <source>Network error</source>
        <translation>Збій мережі</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="503"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="527"/>
        <source>Something went wrong</source>
        <translation>Щось погано вийшло.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="506"/>
        <source>Save file</source>
        <translation>Зберегти файл</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="507"/>
        <source>Open location</source>
        <translation>Відкрити розташування</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="764"/>
        <source>Me</source>
        <translation>Я</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="511"/>
        <source>Install beta version</source>
        <translation>Встановити бета-версію</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="512"/>
        <source>Check for updates now</source>
        <translation>Перевірити на оновлення зараз</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="513"/>
        <source>Enable/Disable automatic updates</source>
        <translation>Дозволити/вимкнути автоматичні оновлення</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="514"/>
        <source>Updates</source>
        <translation>Оновлення</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="515"/>
        <source>Update</source>
        <translation>Оновити</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="517"/>
        <source>No new version of Jami was found</source>
        <translation>Не знайдено нової версії Jami</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="518"/>
        <source>An error occured when checking for a new version</source>
        <translation>Під час перевірки на нову версію сталася помилка</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="520"/>
        <source>SSL error</source>
        <translation>Помилка SSL</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="521"/>
        <source>Installer download canceled</source>
        <translation>Завантаження інсталятора скасовано</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="523"/>
        <source>This will uninstall your current Release version and you can always download the latest Release version on our website</source>
        <translation>Це буде видаляти вашу поточну версію випуску і ви завжди можете завантажити останню версію випуску на нашому сайті</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="524"/>
        <source>Network disconnected</source>
        <translation>З&apos;єднання мережі</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="530"/>
        <source>Troubleshoot</source>
        <translation>Виправлення проблем</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="531"/>
        <source>Open logs</source>
        <translation>Відкриті журнали</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="532"/>
        <source>Get logs</source>
        <translation>Знайди журнали</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="534"/>
        <source>(Experimental) Enable call support for swarm</source>
        <translation>(експіриментальний) Звернути підтримку дзвінків для рогу</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="535"/>
        <source>This feature will enable call buttons in swarms with multiple participants.</source>
        <translation>Ця функція дозволить зателефонувати в рогах з декількома учасниками.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="538"/>
        <source>Quality</source>
        <translation>Якість</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="542"/>
        <source>Always record calls</source>
        <translation>Завжди записуйте дзвінки</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="545"/>
        <source>Keyboard Shortcut Table</source>
        <translation>Таблиця клавіатурних скорочень</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="546"/>
        <source>Keyboard Shortcuts</source>
        <translation>Клавіатурні скорочення</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="547"/>
        <source>Conversation</source>
        <translation>Розмова</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="548"/>
        <source>Call</source>
        <translation>Дзвінок</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="549"/>
        <source>Settings</source>
        <translation>Налаштування</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="553"/>
        <source>Debug</source>
        <translation>Дебаг</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="555"/>
        <source>Report Bug</source>
        <translation>Звіт про недолік</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="556"/>
        <source>Clear</source>
        <translation>Очистити</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="557"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="705"/>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="558"/>
        <source>Copied to clipboard!</source>
        <translation>Скопійовано до буфера обміну!</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="559"/>
        <source>Receive Logs</source>
        <translation>Приймають журнали</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="562"/>
        <source>Archive</source>
        <translation>Архів</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="563"/>
        <source>Open file</source>
        <translation>Відкрити файл</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="566"/>
        <source>Generating account…</source>
        <translation>Побудова рахунку...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="567"/>
        <source>Import from archive backup</source>
        <translation>Імпорт з архивного резервного копію</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="569"/>
        <source>Select archive file</source>
        <translation>Виберіть файл архіва</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="437"/>
        <source>Link device</source>
        <translation>Устрій зв&apos;язку</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="573"/>
        <source>Import</source>
        <translation>Імпортувати</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="575"/>
        <source>A PIN is required to use an existing Jami account on this device.</source>
        <translation>Для використання існуючого облікового запису Jami на цьому пристрої потрібен PIN.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="581"/>
        <source>Choose the account to link</source>
        <translation>Виберіть об&apos;єкт для посилання</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="587"/>
        <source>The PIN and the account password should be entered in your device within 10 minutes.</source>
        <translation>ПИН-код і пароль рахунку повинні бути введені в пристрої протягом 10 хвилин.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="610"/>
        <source>Choose a picture</source>
        <translation>Виберіть фотографію</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="790"/>
        <source>Contact&apos;s name</source>
        <translation>Ім&apos;я контактного особи</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="805"/>
        <source>Reinstate member</source>
        <translation>Відновлення члена</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="819"/>
        <source>Delete message</source>
        <translation>Видалити повідомлення</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="820"/>
        <source>*(Deleted Message)*</source>
        <translation>*(Збито повідомлення) *</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="821"/>
        <source>Edit message</source>
        <translation>Редагувати повідомлення</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="321"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="588"/>
        <source>Close</source>
        <translation>Закрити</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="541"/>
        <source>Call recording</source>
        <translation>Запис дзвінків</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="570"/>
        <source>If the account is encrypted with a password, please fill the following field.</source>
        <translation>Якщо обліковий запис зашифрований паролем, заповніть наступне поле.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="574"/>
        <source>Enter the PIN code</source>
        <translation>Введіть PIN-код</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="576"/>
        <source>Step 01</source>
        <translation>Крок 01</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="577"/>
        <source>Step 02</source>
        <translation>Крок 02</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="578"/>
        <source>Step 03</source>
        <translation>Крок 03</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="579"/>
        <source>Step 04</source>
        <translation>Крок 04</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="580"/>
        <source>Go to the account management settings of a previous device</source>
        <translation>Перейдіть до налаштувань управління рахунком попереднього пристрою</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="583"/>
        <source>The PIN code will be available for 10 minutes</source>
        <translation>Пін-код буде доступний протягом 10 хвилин</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="584"/>
        <source>Fill if the account is password-encrypted.</source>
        <translation>Заповніть, якщо на рахунок шифрується пароль.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="591"/>
        <source>Add Device</source>
        <translation>Додати пристрій</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="601"/>
        <source>Enter current password</source>
        <translation>Введіть поточний пароль</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="603"/>
        <source>Enter new password</source>
        <translation>Введіть новий пароль</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="604"/>
        <source>Confirm new password</source>
        <translation>Підтвердити новий пароль</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="605"/>
        <source>Change</source>
        <translation>Змінити</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="606"/>
        <source>Export</source>
        <translation>Експортувати</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="611"/>
        <source>Import avatar from image file</source>
        <translation>Импорт аватара з файлу зображення</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="612"/>
        <source>Clear avatar image</source>
        <translation>Чисте зображення аватара</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="613"/>
        <source>Take photo</source>
        <translation>Зняти фото</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="626"/>
        <source>Preferences</source>
        <translation>Переваги</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="629"/>
        <source>Reset</source>
        <translation>Скинути</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="630"/>
        <source>Uninstall</source>
        <translation>Видалити</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="631"/>
        <source>Reset Preferences</source>
        <translation>Передумови реформування</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="632"/>
        <source>Select a plugin to install</source>
        <translation>Виберіть плагин для установки</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="633"/>
        <source>Uninstall plugin</source>
        <translation>Видалити втулку</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="634"/>
        <source>Are you sure you wish to reset %1 preferences?</source>
        <translation>Ви впевнені, що хочете реформувати передумови %1?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="635"/>
        <source>Are you sure you wish to uninstall %1?</source>
        <translation>Ви впевнені, що хочете видалити %1?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="636"/>
        <source>Go back to plugins list</source>
        <translation>Поверніться до списку вставників</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="637"/>
        <source>Select a file</source>
        <translation>Обрати файл</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="119"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="638"/>
        <source>Select</source>
        <translation>Виберіть</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="639"/>
        <source>Choose image file</source>
        <translation>Обрати файл зображення</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="640"/>
        <source>Plugin Files (*.jpl)</source>
        <translation>Файли втулок (*.jpl)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="641"/>
        <source>Load/Unload</source>
        <translation>Завантаження/вивантаження</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="642"/>
        <source>Select An Image to %1</source>
        <translation>Виберіть зображення до % 1</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="643"/>
        <source>Edit preference</source>
        <translation>Редагувати передумови</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="644"/>
        <source>On/Off</source>
        <translation>Включення/включення</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="645"/>
        <source>Choose Plugin</source>
        <translation>Обрати втулку</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="651"/>
        <source>Information</source>
        <translation>Інформація</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="653"/>
        <source>Profile</source>
        <translation>Профіль</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="656"/>
        <source>Enter the account password to confirm the removal of this device</source>
        <translation>Введіть пароль для підтвердження видалення цього пристрою</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="659"/>
        <source>Select a screen to share</source>
        <translation>Виберіть екран, щоб поділитися</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="660"/>
        <source>Select a window to share</source>
        <translation>Виберіть вікно для обміну</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="661"/>
        <source>All Screens</source>
        <translation>Усі екрани</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="662"/>
        <source>Screens</source>
        <translation>Екрани</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="663"/>
        <source>Windows</source>
        <translation>Вікна</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="664"/>
        <source>Screen %1</source>
        <translation>Екран %1</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="667"/>
        <source>QR code</source>
        <translation>QR-код</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="670"/>
        <source>Link this device to an existing account</source>
        <translation>Зв&apos;язати цей пристрій з наявним обліковим записом</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="671"/>
        <source>Import from another device</source>
        <translation>Імпорт з іншого пристрою</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="672"/>
        <source>Import from an archive backup</source>
        <translation>Імпорт з резервної копії архівів</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="673"/>
        <source>Advanced features</source>
        <translation>Розширені можливості</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="674"/>
        <source>Show advanced features</source>
        <translation>Показати розширені можливості</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="675"/>
        <source>Hide advanced features</source>
        <translation>Сховати розширені можливості</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="676"/>
        <source>Connect to a JAMS server</source>
        <translation>Підключення до сервера JAMS</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="677"/>
        <source>Create account from Jami Account Management Server (JAMS)</source>
        <translation>Створити обліковий запис зі сервера керування обліковими записами Jami (JAMS)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="678"/>
        <source>Configure a SIP account</source>
        <translation>Конфігуруйте SIP-рахунк</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="679"/>
        <source>Error while creating your account. Check your credentials.</source>
        <translation>Помилка при створенні вашого облікового запису.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="680"/>
        <source>Create a rendezvous point</source>
        <translation>Створити точку рандеву</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="681"/>
        <source>Join Jami</source>
        <translation>Приєднуйся до Jami</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="682"/>
        <source>Create new Jami account</source>
        <translation>Створення нового облікового запису Jami</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="683"/>
        <source>Create new SIP account</source>
        <translation>Створення нового SIP-околу</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="684"/>
        <source>About Jami</source>
        <translation>Про Jami</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="686"/>
        <source>I already have an account</source>
        <translation>У мене вже є рахунок.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="687"/>
        <source>Use existing Jami account</source>
        <translation>Використовуйте існуючий акаунт Jami</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="688"/>
        <source>Welcome to Jami</source>
        <translation>Вітаємо в Jami</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="691"/>
        <source>Clear Text</source>
        <translation>Очистити текст</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="692"/>
        <source>Conversations</source>
        <translation>Групові чати</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="693"/>
        <source>Search Results</source>
        <translation>Результати пошуку</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="696"/>
        <source>Decline contact request</source>
        <translation>Запрос контакту відмовлятися</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="697"/>
        <source>Accept contact request</source>
        <translation>Прийміть запит на контакт</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="700"/>
        <source>Automatically check for updates</source>
        <translation>Автоматично перевіряти оновлення</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="703"/>
        <source>Ok</source>
        <translation>Гаразд</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="463"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="704"/>
        <source>Save</source>
        <translation>Зберігти</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="706"/>
        <source>Upgrade</source>
        <translation>Повернення</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="707"/>
        <source>Later</source>
        <translation>Пізніше</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="708"/>
        <source>Delete</source>
        <translation>Видалити</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="710"/>
        <source>Block</source>
        <translation>Заблокувати</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="714"/>
        <source>Set moderator</source>
        <translation>Наведіть модератор</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="715"/>
        <source>Unset moderator</source>
        <translation>Не встановлюється модератор</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="317"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="718"/>
        <source>Maximize</source>
        <translation>Збільшити</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="719"/>
        <source>Minimize</source>
        <translation>Зменьшити</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="720"/>
        <source>Hangup</source>
        <translation>Відновлення виклику</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="721"/>
        <source>Local muted</source>
        <translation>Місцеві затишні</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="724"/>
        <source>Default moderators</source>
        <translation>Позадумовні модератори</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="725"/>
        <source>Enable local moderators</source>
        <translation>Отримати локальні модератори</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="726"/>
        <source>Make all participants moderators</source>
        <translation>Зробіть усіх учасників модераторами</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="727"/>
        <source>Add default moderator</source>
        <translation>Додати модератора за замовчуванням</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="729"/>
        <source>Remove default moderator</source>
        <translation>Вилучити дефолтний модератор</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="736"/>
        <source>Add emoji</source>
        <translation>Додати emoji</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="738"/>
        <source>Send file</source>
        <translation>Відправити файл</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="760"/>
        <source>Send</source>
        <translation>Відправити</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="466"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="709"/>
        <source>Remove</source>
        <translation>Видалити</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="41"/>
        <source>Migrate conversation</source>
        <translation>Переїзд розмови</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="475"/>
        <source>Show notifications</source>
        <translation>Показувати сповіщення</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="476"/>
        <source>Minimize on close</source>
        <translation>Минімізувати на ближчі</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="477"/>
        <source>Run at system startup</source>
        <translation>Запускати під час запуску системи</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="564"/>
        <source>Create account from backup</source>
        <translation>Створити обліковий запис із резервної копії</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="565"/>
        <source>Restore account from backup</source>
        <translation>Відновити обліковий запис із резервної копії</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="568"/>
        <source>Import Jami account from local archive file.</source>
        <translation>Імпортувати обліковий запис Jami з локального архівного файлу.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="614"/>
        <source>Image Files (*.png *.jpg *.jpeg *.JPG *.JPEG *.PNG)</source>
        <translation>Файли зображень (*.png *.jpg *.jpeg *.JPG *.JPEG *.PNG)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="766"/>
        <source>Write to %1</source>
        <translation>Напишіть на %1</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="781"/>
        <source>%1 has sent you a request for a conversation.</source>
        <translation>%1 надіслав вам запит на розмову.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="782"/>
        <source>Hello,
Would you like to join the conversation?</source>
        <translation>Привіт, чи не хочеш приєднатися до розмови?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="783"/>
        <source>You have accepted
the conversation request</source>
        <translation>Ви прийняли запит на розмову</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="784"/>
        <source>Waiting until %1
connects to synchronize the conversation.</source>
        <translation>Чакаючи, поки %1 не підключить, щоб синхронізувати розмову.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="787"/>
        <source>%1 Members</source>
        <translation>%1 Члени</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="788"/>
        <source>Member</source>
        <translation>Член</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="789"/>
        <source>Swarm&apos;s name</source>
        <translation>Назва Сварма</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="791"/>
        <source>Add a description</source>
        <translation>Додати опис</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="794"/>
        <source>Ignore all notifications from this conversation</source>
        <translation>Ігнорувати всі сповіщення з цієї розмови</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="795"/>
        <source>Choose a color</source>
        <translation>Обрати колір</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="797"/>
        <source>Leave conversation</source>
        <translation>Не переконвайтеся</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="798"/>
        <source>Type of swarm</source>
        <translation>Тип рогу</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="802"/>
        <source>Create the swarm</source>
        <translation>Створюйте ров</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="803"/>
        <source>Go to conversation</source>
        <translation>Пойди на розмову.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="804"/>
        <source>Kick member</source>
        <translation>Членки &quot;Кік&quot;</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="806"/>
        <source>Administrator</source>
        <translation>Адміністратор</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="807"/>
        <source>Invited</source>
        <translation>Запрошують</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="808"/>
        <source>Remove member</source>
        <translation>Вилучити члена</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="809"/>
        <source>To:</source>
        <translation>Для:</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="812"/>
        <source>Customize</source>
        <translation>Натомість</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="814"/>
        <source>Dismiss</source>
        <translation>Відхилити</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="816"/>
        <source>Your profile is only shared with your contacts</source>
        <translation>Ваш профіль бачитимуть лише ваші контакти</translation>
    </message>
</context>
<context>
    <name>KeyboardShortcutTable</name>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="41"/>
        <source>Open account list</source>
        <translation>Відкрити список облікових записів</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="45"/>
        <source>Focus conversations list</source>
        <translation>Список розмов</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="49"/>
        <source>Requests list</source>
        <translation>Список запитів</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="53"/>
        <source>Previous conversation</source>
        <translation>Раніше розмова</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="57"/>
        <source>Next conversation</source>
        <translation>Наступна розмова</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="61"/>
        <source>Search bar</source>
        <translation>Бар пошуку</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="65"/>
        <source>Full screen</source>
        <translation>На весь екран</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="69"/>
        <source>Increase font size</source>
        <translation>Збільшити розмір шрифту</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="73"/>
        <source>Decrease font size</source>
        <translation>Зменшити розмір шрифту</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="77"/>
        <source>Reset font size</source>
        <translation>Скинути розмір шрифту</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="85"/>
        <source>Start an audio call</source>
        <translation>Розпочати аудіо дзвінок</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="89"/>
        <source>Start a video call</source>
        <translation>Розпочати відео дзвінок</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="93"/>
        <source>Clear history</source>
        <translation>Видалити історію</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="97"/>
        <source>Search messages/files</source>
        <translation>Пошук повідомлень/файлів</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="101"/>
        <source>Block contact</source>
        <translation>Заблокувати контакт</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="105"/>
        <source>Remove conversation</source>
        <translation>Вилучити розмову</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="109"/>
        <source>Accept contact request</source>
        <translation>Прийміть запит на контакт</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="113"/>
        <source>Edit last message</source>
        <translation>Редагуйте останнє повідомлення</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="117"/>
        <source>Cancel message edition</source>
        <translation>Відкласти редакцію повідомлення</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="125"/>
        <source>Media settings</source>
        <translation>Настройки медіа</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="129"/>
        <source>General settings</source>
        <translation>Загальні налаштування</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="133"/>
        <source>Account settings</source>
        <translation>Налаштування облікового запису</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="137"/>
        <source>Plugin settings</source>
        <translation>Налаштування втулки</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="141"/>
        <source>Open account creation wizard</source>
        <translation>Виконник створення рахунків</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="146"/>
        <source>Open keyboard shortcut table</source>
        <translation>Відкрити таблицю клавіатурних скорочень</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="154"/>
        <source>Answer an incoming call</source>
        <translation>Відповідь на прийдешній дзвін</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="158"/>
        <source>End call</source>
        <translation>Закінчити дзвінок</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="162"/>
        <source>Decline the call request</source>
        <translation>Відмова від запиту на дзвінки</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="166"/>
        <source>Mute microphone</source>
        <translation>Відключити мікрофон</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="170"/>
        <source>Stop camera</source>
        <translation>Зупинити камеру</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="174"/>
        <source>Take tile screenshot</source>
        <translation>Зробіть скриншот плитки</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="182"/>
        <source>Bold</source>
        <translation>Жирний</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="186"/>
        <source>Italic</source>
        <translation>Курсив</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="190"/>
        <source>Strikethrough</source>
        <translation>Закреслений</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="194"/>
        <source>Heading</source>
        <translation>Заголовок</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="198"/>
        <source>Link</source>
        <translation>Зв&apos;язати</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="202"/>
        <source>Code</source>
        <translation>Код</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="206"/>
        <source>Quote</source>
        <translation>Цитата</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="210"/>
        <source>Unordered list</source>
        <translation>Невпорядкований список</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="214"/>
        <source>Ordered list</source>
        <translation>Упорядкований список</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="218"/>
        <source>Show formatting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="222"/>
        <source>Show preview</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>MainApplication</name>
    <message>
        <location filename="../src/app/mainapplication.cpp" line="375"/>
        <source>E&amp;xit</source>
        <translation>Е&amp;ксит</translation>
    </message>
    <message>
        <location filename="../src/app/mainapplication.cpp" line="377"/>
        <source>&amp;Quit</source>
        <translation>&amp;Вихід</translation>
    </message>
    <message>
        <location filename="../src/app/mainapplication.cpp" line="383"/>
        <source>&amp;Show Jami</source>
        <translation>&amp;Show Jami</translation>
    </message>
</context>
<context>
    <name>PositionManager</name>
    <message>
        <location filename="../src/app/positionmanager.cpp" line="414"/>
        <source>%1 is sharing their location</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/positionmanager.cpp" line="419"/>
        <source>Location sharing</source>
        <translation>Розділ місцезнаходження</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/libclient/qtwrapper/callmanager_wrap.h" line="458"/>
        <source>Me</source>
        <translation>Я</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="62"/>
        <source>Hold</source>
        <translation>Режим очікування</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="64"/>
        <source>Talking</source>
        <translation>Розмовляє</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="66"/>
        <source>ERROR</source>
        <translation>ПОМИЛКА</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="68"/>
        <source>Incoming</source>
        <translation>Викликає</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="70"/>
        <source>Calling</source>
        <translation>Дзвінок</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="373"/>
        <location filename="../src/libclient/api/call.h" line="72"/>
        <source>Connecting</source>
        <translation>З&apos;єднання</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="74"/>
        <source>Searching</source>
        <translation>Шукаю</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="76"/>
        <source>Inactive</source>
        <translation>Неактивний</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="390"/>
        <location filename="../src/libclient/api/call.h" line="78"/>
        <location filename="../src/libclient/api/call.h" line="84"/>
        <source>Finished</source>
        <translation>Вийшло</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="80"/>
        <source>Timeout</source>
        <translation>Час відключення</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="82"/>
        <source>Peer busy</source>
        <translation>Співробітники зайняті</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="86"/>
        <source>Communication established</source>
        <translation>Зв’язок встановлено</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="211"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="994"/>
        <source>Invitation received</source>
        <translation>Отримано запрошення</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="260"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="208"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="992"/>
        <source>Contact added</source>
        <translation>Контакт додано</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="262"/>
        <source>%1 was invited to join</source>
        <translation>%1 запрошений приєднатися</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="264"/>
        <source>%1 joined</source>
        <translation>%1 приєднується</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="266"/>
        <source>%1 left</source>
        <translation>Користувач %1 вийшов</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="268"/>
        <source>%1 was kicked</source>
        <translation>%1 був відкинутий</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="270"/>
        <source>%1 was re-added</source>
        <translation>%1 було додано повторно</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="359"/>
        <source>Private conversation created</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="361"/>
        <source>Swarm created</source>
        <translation>Створена пастка</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="174"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="180"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="990"/>
        <source>Outgoing call</source>
        <translation>Вихідний виклик</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="176"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="186"/>
        <source>Incoming call</source>
        <translation>Вхідний виклик</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="182"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="988"/>
        <source>Missed outgoing call</source>
        <translation>Пропущений вихідний виклик</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="188"/>
        <source>Missed incoming call</source>
        <translation>Пропущений вхідний виклик</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="198"/>
        <source>Join call</source>
        <translation>Приєднати виклик</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="213"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="996"/>
        <source>Invitation accepted</source>
        <translation>Запрошення прийнято</translation>
    </message>
    <message>
        <location filename="../src/libclient/avmodel.cpp" line="391"/>
        <location filename="../src/libclient/avmodel.cpp" line="410"/>
        <source>default</source>
        <translation>за замовчуванням</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="72"/>
        <source>Null</source>
        <translation>Ні</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="73"/>
        <source>Trying</source>
        <translation>Спробую</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="74"/>
        <source>Ringing</source>
        <translation>Дзвінок</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="75"/>
        <source>Being Forwarded</source>
        <translation>Відправляються</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="76"/>
        <source>Queued</source>
        <translation>Засіда</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="77"/>
        <source>Progress</source>
        <translation>Прогрес</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="78"/>
        <source>OK</source>
        <translation>Гаразд</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="79"/>
        <source>Accepted</source>
        <translation>Прийнято</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="80"/>
        <source>Multiple Choices</source>
        <translation>Багато варіантів</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="81"/>
        <source>Moved Permanently</source>
        <translation>Переміщення постійно</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="82"/>
        <source>Moved Temporarily</source>
        <translation>Переїзд тимчасово</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="83"/>
        <source>Use Proxy</source>
        <translation>Використовуйте прокси</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="84"/>
        <source>Alternative Service</source>
        <translation>Альтернативний сервіс</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="85"/>
        <source>Bad Request</source>
        <translation>Неправильний запит</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="86"/>
        <source>Unauthorized</source>
        <translation>Неавторизовано</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="87"/>
        <source>Payment Required</source>
        <translation>Заможна оплата</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="88"/>
        <source>Forbidden</source>
        <translation>Заборонено</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="89"/>
        <source>Not Found</source>
        <translation>Не знайдено</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="90"/>
        <source>Method Not Allowed</source>
        <translation>Метод не допускається</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="91"/>
        <location filename="../src/libclient/callmodel.cpp" line="111"/>
        <source>Not Acceptable</source>
        <translation>Не прийнято</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="92"/>
        <source>Proxy Authentication Required</source>
        <translation>Необхідна аутентифікація прокси</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="93"/>
        <source>Request Timeout</source>
        <translation>Очікування після запросу</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="94"/>
        <source>Gone</source>
        <translation>Зникли</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="95"/>
        <source>Request Entity Too Large</source>
        <translation>Запрос за великою судною</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="96"/>
        <source>Request URI Too Long</source>
        <translation>Запрошуйте URI занадто довго</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="97"/>
        <source>Unsupported Media Type</source>
        <translation>Тип медіа, який не підтримується</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="98"/>
        <source>Unsupported URI Scheme</source>
        <translation>Непідтримувана схема URI</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="99"/>
        <source>Bad Extension</source>
        <translation>Погано розширення</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="100"/>
        <source>Extension Required</source>
        <translation>Необхідно продовжити</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="101"/>
        <source>Session Timer Too Small</source>
        <translation>Час засідання занадто маленький</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="102"/>
        <source>Interval Too Brief</source>
        <translation>Перестань занадто короткий</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="103"/>
        <source>Temporarily Unavailable</source>
        <translation>Недоступна тимчасово</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="104"/>
        <source>Call TSX Does Not Exist</source>
        <translation>Зов TSX не існує</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="105"/>
        <source>Loop Detected</source>
        <translation>З&apos;ясувався залом</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="106"/>
        <source>Too Many Hops</source>
        <translation>Скільки скоків</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="107"/>
        <source>Address Incomplete</source>
        <translation>Адреса Неповні</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="108"/>
        <source>Ambiguous</source>
        <translation>Неіднозначний</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="109"/>
        <source>Busy</source>
        <translation>Зайнятий</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="110"/>
        <source>Request Terminated</source>
        <translation>Запрос завершений</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="112"/>
        <source>Bad Event</source>
        <translation>Погано сталося</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="113"/>
        <source>Request Updated</source>
        <translation>Оновлено запит</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="114"/>
        <source>Request Pending</source>
        <translation>Запрос чекає</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="115"/>
        <source>Undecipherable</source>
        <translation>Нерозрозуміло.</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="116"/>
        <source>Internal Server Error</source>
        <translation>Помилка внутрішнього сервера</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="117"/>
        <source>Not Implemented</source>
        <translation>Не виконано</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="118"/>
        <source>Bad Gateway</source>
        <translation>Злой шлюз</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="119"/>
        <source>Service Unavailable</source>
        <translation>Служба недоступна</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="120"/>
        <source>Server Timeout</source>
        <translation>Час відключення сервера</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="121"/>
        <source>Version Not Supported</source>
        <translation>Версія Непідтримується</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="122"/>
        <source>Message Too Large</source>
        <translation>Празник занадто великий</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="123"/>
        <source>Precondition Failure</source>
        <translation>Невдачі попередніх умов</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="124"/>
        <source>Busy Everywhere</source>
        <translation>Завантажені всюди</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="125"/>
        <source>Call Refused</source>
        <translation>Зов&apos;язок відмовлений</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="126"/>
        <source>Does Not Exist Anywhere</source>
        <translation>Ніде не існує</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="127"/>
        <source>Not Acceptable Anywhere</source>
        <translation>Ніде не прийнято</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="375"/>
        <source>Accept</source>
        <translation>Прийняти</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="367"/>
        <source>Sending</source>
        <translation>Відправлення</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="371"/>
        <source>Sent</source>
        <translation>Відправлено</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="380"/>
        <source>Unable to make contact</source>
        <translation>Не змогла зв&apos;язатися</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="384"/>
        <source>Waiting for contact</source>
        <translation>Очікуючи контакту</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="386"/>
        <source>Incoming transfer</source>
        <translation>Прихідний трансфер</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="388"/>
        <source>Timed out waiting for contact</source>
        <translation>Почастий, що чекають контакту</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="742"/>
        <source>Today</source>
        <translation>Сьогодні</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="744"/>
        <source>Yesterday</source>
        <translation>Вчора</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="377"/>
        <source>Canceled</source>
        <translation>Скасовано</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="382"/>
        <source>Ongoing</source>
        <translation>Продовження</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="723"/>
        <source>just now</source>
        <translation>щойно</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="369"/>
        <source>Failure</source>
        <translation>Невдача</translation>
    </message>
    <message>
        <location filename="../src/app/positioning.cpp" line="109"/>
        <source>locationServicesError</source>
        <translation>Розміщення Послуги Помилка</translation>
    </message>
    <message>
        <location filename="../src/app/positioning.cpp" line="112"/>
        <source>locationServicesClosedError</source>
        <translation>РозміщенняСервіси ЗаключенийВипадок</translation>
    </message>
    <message>
        <location filename="../src/app/positioning.cpp" line="114"/>
        <source>locationServicesUnknownError</source>
        <translation>Розміщення ПослугиНевідомоПошибка</translation>
    </message>
    <message>
        <location filename="../src/libclient/conversationmodel.cpp" line="1166"/>
        <location filename="../src/libclient/conversationmodel.cpp" line="1179"/>
        <source>%1 (you)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SmartListModel</name>
    <message>
        <location filename="../src/app/smartlistmodel.cpp" line="68"/>
        <location filename="../src/app/smartlistmodel.cpp" line="107"/>
        <location filename="../src/app/smartlistmodel.cpp" line="115"/>
        <location filename="../src/app/smartlistmodel.cpp" line="161"/>
        <location filename="../src/app/smartlistmodel.cpp" line="191"/>
        <location filename="../src/app/smartlistmodel.cpp" line="192"/>
        <source>Calls</source>
        <translation>Дзвінки</translation>
    </message>
    <message>
        <location filename="../src/app/smartlistmodel.cpp" line="69"/>
        <location filename="../src/app/smartlistmodel.cpp" line="108"/>
        <location filename="../src/app/smartlistmodel.cpp" line="125"/>
        <location filename="../src/app/smartlistmodel.cpp" line="162"/>
        <location filename="../src/app/smartlistmodel.cpp" line="193"/>
        <location filename="../src/app/smartlistmodel.cpp" line="194"/>
        <source>Contacts</source>
        <translation>Контакти</translation>
    </message>
</context>
<context>
    <name>SystemTray</name>
    <message>
        <location filename="../src/app/systemtray.cpp" line="216"/>
        <source>Answer</source>
        <translation>Відповідь</translation>
    </message>
    <message>
        <location filename="../src/app/systemtray.cpp" line="217"/>
        <source>Decline</source>
        <translation>Відхилити</translation>
    </message>
    <message>
        <location filename="../src/app/systemtray.cpp" line="219"/>
        <source>Open conversation</source>
        <translation>Відкрити розмову</translation>
    </message>
    <message>
        <location filename="../src/app/systemtray.cpp" line="221"/>
        <source>Accept</source>
        <translation>Прийняти</translation>
    </message>
    <message>
        <location filename="../src/app/systemtray.cpp" line="222"/>
        <source>Refuse</source>
        <translation>Скидання</translation>
    </message>
</context>
<context>
    <name>TipsModel</name>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="85"/>
        <source>Customize</source>
        <translation>Натомість</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="88"/>
        <source>What does Jami mean?</source>
        <translation>Що означає Jami?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="94"/>
        <source>What is the green dot next to my account?</source>
        <translation>Яка зелена точка поруч з моєю рахунком?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="90"/>
        <source>The choice of the name Jami was inspired by the Swahili word &apos;jamii&apos;, which means &apos;community&apos; as a noun and &apos;together&apos; as an adverb.</source>
        <translation>Вибір імені Jami був натхненний словом &quot;jamii&quot; на суахілі, що означає &quot;спільнота&quot; як ім&apos;я і &quot;з&apos;єднатися&quot; як дієслово.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="81"/>
        <source>Donate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="86"/>
        <source>Backup account</source>
        <translation>Резервна копія облікового запису</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="96"/>
        <source>A red dot means that your account is disconnected from the network; it turns green when it&apos;s connected.</source>
        <translation>Червона точка означає, що ваш рахунок відключений від мережі; він стає зеленого, коли він підключений.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="101"/>
        <source>Why should I back up my account?</source>
        <translation>Чому мені потрібно створити резервну копію свого облікового запису?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="103"/>
        <source>Jami is distributed and your account is only stored locally on your device. If you lose your password or your local account data, you WILL NOT be able to recover your account if you did not back it up earlier.</source>
        <translation>Якщо ви втратите пароль або дані місцевого рахунку, ви НЕ зможете відновити свій рахунок, якщо не зробили його запіс раніше.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="109"/>
        <source>Can I make a conference call?</source>
        <translation>Я можу зробити конференц-звон?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="114"/>
        <source>What is a Jami account?</source>
        <translation>Що таке рахунок Jami?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="116"/>
        <source>A Jami account is an asymmetric encryption key. Your account is identified by a Jami ID, which is a fingerprint of your public key.</source>
        <translation>Аккаунт Jami - це асиметричний шифрований ключ. Ваш рахунок ідентифікується ідентифікатором Jami, який є відбитком пальців вашого публічного ключа.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="120"/>
        <source>What information do I need to provide to create a Jami account?</source>
        <translation>Яку інформацію мені потрібно надати для створення рахунку Jami?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="122"/>
        <source>When you create a new Jami account, you do not have to provide any private information like an email, address, or phone number.</source>
        <translation>Коли ви створюєте новий акаунт Jami, вам не потрібно надавати ніякої особистий інформації, наприклад електронну пошту, адресу або номер телефону.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="129"/>
        <source>With Jami, your account is stored in a directory on your device. The password is only used to encrypt your account in order to protect you from someone who has physical access to your device.</source>
        <translation>У випадку з Jami, ваш акаунт зберігається в каталозі на вашому пристрої.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="149"/>
        <source>Your account is only stored on your own devices. If you delete your account from all of your devices, the account is gone forever and you CANNOT recover it.</source>
        <translation>Якщо ви видалите свій обліковий запис з усіх своїх пристроїв, він назавжди зник і ви НЕ зможете його відновити.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="153"/>
        <source>Can I use my account on multiple devices?</source>
        <translation>Я можу використовувати свій аккаунт на декількох пристроях?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="155"/>
        <source>Yes, you can link your account from the settings, or you can import your backup on another device.</source>
        <translation>Так, ви можете пов&apos;язати свій обліковий запис з налаштувань або імпортувати свій резервний резерв на інше пристрої.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="127"/>
        <source>Why don&apos;t I have to use a password?</source>
        <translation>Чому мені не потрібно використовувати пароль?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="111"/>
        <source>In a call, you can click on &quot;Add participants&quot; to add a contact to a call.</source>
        <translation>У дзвінку ви можете натиснути на &quot;Додайте учасників&quot;, щоб додати контакту до дзвінку.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="135"/>
        <source>Why don&apos;t I have to register a username?</source>
        <translation>Чому мені не потрібно зареєструвати ім&apos;я користувача?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="137"/>
        <source>The most permanent, secure identifier is your Jami ID, but since these are difficult to use for some people, you also have the option of registering a username.</source>
        <translation>Найпостійший і безпечний ідентифікатор - ідентифікатор Jami, але оскільки для деяких людей це важко використовувати, ви також можете зареєструвати ім&apos;я користувача.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="142"/>
        <source>How can I back up my account?</source>
        <translation>Як я можу створити резервну копію свого облікового запису?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="143"/>
        <source>In Account Settings, a button is available to create a backup your account.</source>
        <translation>У парафілі налаштування рахунку є кнопка для створення резервної копії вашого рахунку.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="147"/>
        <source>What happens when I delete my account?</source>
        <translation>Що станеться, коли я видалю свій аккаунт?</translation>
    </message>
</context>
<context>
    <name>UtilsAdapter</name>
    <message>
        <location filename="../src/app/utilsadapter.cpp" line="334"/>
        <source>%1 Mbps</source>
        <translation>%1 Мбіт/с</translation>
    </message>
    <message>
        <location filename="../src/app/utilsadapter.cpp" line="334"/>
        <source>Default</source>
        <translation>За замовчуванням</translation>
    </message>
    <message>
        <location filename="../src/app/utilsadapter.cpp" line="539"/>
        <source>System</source>
        <translation>Система</translation>
    </message>
</context>
<context>
    <name>lrc::ContactModelPimpl</name>
    <message>
        <location filename="../src/libclient/contactmodel.cpp" line="478"/>
        <source>Searching…</source>
        <translation>Пошук…</translation>
    </message>
    <message>
        <location filename="../src/libclient/contactmodel.cpp" line="1015"/>
        <source>Invalid ID</source>
        <translation>Недійний ідентифікатор</translation>
    </message>
    <message>
        <location filename="../src/libclient/contactmodel.cpp" line="1018"/>
        <source>Username not found</source>
        <translation>Назва користувача не знайдена</translation>
    </message>
    <message>
        <location filename="../src/libclient/contactmodel.cpp" line="1021"/>
        <source>Couldn&apos;t lookup…</source>
        <translation>Не міг шукати...</translation>
    </message>
</context>
<context>
    <name>lrc::api::ContactModel</name>
    <message>
        <location filename="../src/libclient/contactmodel.cpp" line="441"/>
        <source>Bad URI scheme</source>
        <translation>Погано схема URI</translation>
    </message>
</context>
</TS>