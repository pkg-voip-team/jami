<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="da" sourcelanguage="en">
<context>
    <name>CallAdapter</name>
    <message>
        <location filename="../src/app/calladapter.cpp" line="220"/>
        <source>Missed call</source>
        <translation>Ubesvaret opkld</translation>
    </message>
    <message>
        <location filename="../src/app/calladapter.cpp" line="221"/>
        <source>Missed call with %1</source>
        <translation>Ubesvaret opkald fra %1</translation>
    </message>
    <message>
        <location filename="../src/app/calladapter.cpp" line="542"/>
        <source>Incoming call</source>
        <translation>Indgående opkald</translation>
    </message>
    <message>
        <location filename="../src/app/calladapter.cpp" line="543"/>
        <source>%1 is calling you</source>
        <translation>%1 ringer</translation>
    </message>
    <message>
        <location filename="../src/app/calladapter.cpp" line="554"/>
        <source>is calling you</source>
        <translation>ringer</translation>
    </message>
    <message>
        <location filename="../src/app/calladapter.cpp" line="1052"/>
        <source>Screenshot</source>
        <translation>Skærmbillede</translation>
    </message>
</context>
<context>
    <name>ConversationsAdapter</name>
    <message>
        <location filename="../src/app/conversationsadapter.cpp" line="189"/>
        <source>%1 received a new message</source>
        <translation>% 1 modtog en ny besked</translation>
    </message>
    <message>
        <location filename="../src/app/conversationsadapter.cpp" line="244"/>
        <source>%1 received a new trust request</source>
        <translation>%1 modtog en ny tillidsansøgning</translation>
    </message>
</context>
<context>
    <name>CurrentCall</name>
    <message>
        <location filename="../src/app/currentcall.cpp" line="185"/>
        <source>Me</source>
        <translation>Mig</translation>
    </message>
</context>
<context>
    <name>CurrentConversation</name>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="137"/>
        <source>Private</source>
        <translation>Privat</translation>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="139"/>
        <source>Private group (restricted invites)</source>
        <translation>Privatgruppe (undtagen invitationer)</translation>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="141"/>
        <source>Private group</source>
        <translation>Privatgruppe</translation>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="143"/>
        <source>Public group</source>
        <translation>Den offentlige gruppe</translation>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="324"/>
        <source>An error occurred while fetching this repository</source>
        <translation>Der er sket et fejltagelse ved at hente dette arkiv</translation>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="326"/>
        <source>Unrecognized conversation mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="330"/>
        <source>Not authorized to update conversation information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="332"/>
        <source>An error occurred while committing a new message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/currentconversation.cpp" line="328"/>
        <source>An invalid message was detected</source>
        <translation>En ugyldig besked er opdaget.</translation>
    </message>
</context>
<context>
    <name>JamiStrings</name>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="29"/>
        <source>Accept</source>
        <translation>Accepter</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="30"/>
        <source>Accept in audio</source>
        <translation>Akcepter i lyd</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="31"/>
        <source>Accept in video</source>
        <translation>Akcepter i video</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="32"/>
        <source>Refuse</source>
        <translation>Afvis</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="33"/>
        <source>End call</source>
        <translation>Afslut opkald</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="34"/>
        <source>Incoming audio call from {}</source>
        <translation>Indkommende lydopkald fra {}</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="35"/>
        <source>Incoming video call from {}</source>
        <translation>Indkommende videoopkald fra {}</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="38"/>
        <source>Invitations</source>
        <translation>Invitationer</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="39"/>
        <source>Jami is a universal communication platform, with privacy as its foundation, that relies on a free distributed network for everyone.</source>
        <translation>Jami er en universel kommunikationsplatform, med privatliv som grund, der er baseret på et gratis distribueret netværk for alle.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="40"/>
        <source>Migrating to the Swarm technology will enable synchronizing this conversation across multiple devices and improve reliability. The legacy conversation history will be cleared in the process.</source>
        <translation>Migrering til Swarm-teknologien vil gøre det muligt at synkronisere denne samtale på tværs af flere enheder og forbedre pålideligheden.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="44"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="733"/>
        <source>Could not re-connect to the Jami daemon (jamid).
Jami will now quit.</source>
        <translation>Jeg kunne ikke få kontakt til Jami-demonen.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="45"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="732"/>
        <source>Trying to reconnect to the Jami daemon (jamid)…</source>
        <translation>Jeg prøver at få kontakt med Jami-demonen.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="48"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="51"/>
        <source>Jami is a free universal communication software that respects the freedom and privacy of its users.</source>
        <translation>Jami er en gratis, universel kommunikationssoftware, der respekterer brugernes frihed og privatliv.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="54"/>
        <source>Display QR code</source>
        <translation>Vis QR-koder</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="55"/>
        <source>Open settings</source>
        <translation>Åbne indstillinger</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="56"/>
        <source>Close settings</source>
        <translation>Tæt indstillinger</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="57"/>
        <source>Add Account</source>
        <translation>Tilføj konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="60"/>
        <source>Add to conference</source>
        <translation>Tilføj til konference</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="61"/>
        <source>Add to conversation</source>
        <translation>Tilføj til samtalen</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="62"/>
        <source>Transfer this call</source>
        <translation>Overfør denne samtale</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="63"/>
        <source>Transfer to</source>
        <translation>Overfør til</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="66"/>
        <source>Authentication required</source>
        <translation>Oplysninger om godkendelse</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="67"/>
        <source>Your session has expired or been revoked on this device. Please enter your password.</source>
        <translation>Din session er udløbet eller er blevet ophævet på denne enhed.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="68"/>
        <source>JAMS server</source>
        <translation>JAMS-server</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="69"/>
        <source>Authenticate</source>
        <translation>Authenticitet</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="70"/>
        <source>Delete account</source>
        <translation>Slet konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="71"/>
        <source>In progress…</source>
        <translation>- I gang med...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="72"/>
        <source>Authentication failed</source>
        <translation>Autentificering fejlede</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="73"/>
        <source>Password</source>
        <translation>Adgangskode</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="74"/>
        <source>Username</source>
        <translation>Brugernavn</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="75"/>
        <source>Alias</source>
        <translation>Alias</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="78"/>
        <source>Allow incoming calls from unknown contacts</source>
        <translation>Tillad indkomne opkald fra ukendte kontakter</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="79"/>
        <source>Convert your account into a rendezvous point</source>
        <translation>Omdann din konto til et mødested</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="80"/>
        <source>Automatically answer calls</source>
        <translation>Automatisk besvar opkald</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="81"/>
        <source>Enable custom ringtone</source>
        <translation>Aktiverer brugerdefineret ringtone</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="82"/>
        <source>Select custom ringtone</source>
        <translation>Vælg brugerdefineret ringtone</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="83"/>
        <source>Select a new ringtone</source>
        <translation>Vælg en ny ringtone</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="84"/>
        <source>Certificate File (*.crt)</source>
        <translation>Certifikatfil (*.crt)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="85"/>
        <source>Audio File (*.wav *.ogg *.opus *.mp3 *.aiff *.wma)</source>
        <translation>Audiofil (*.wav *.ogg *.opus *.mp3 *.aiff *.wma)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="86"/>
        <source>Push-to-talk</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="87"/>
        <source>Enable push-to-talk</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="88"/>
        <source>Keyboard shortcut</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="89"/>
        <source>Change keyboard shortcut</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="92"/>
        <source>Change shortcut</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="93"/>
        <source>Press the key to be assigned to push-to-talk shortcut</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="94"/>
        <source>Assign</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="97"/>
        <source>Enable read receipts</source>
        <translation>Aktivere indlæse kvitteringer</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="98"/>
        <source>Send and receive receipts indicating that a message have been displayed</source>
        <translation>Send og modtage kvitteringer, der angiver, at der er vist en besked</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="101"/>
        <source>Voicemail</source>
        <translation>Telefonsvarer</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="102"/>
        <source>Voicemail dial code</source>
        <translation>Rådsendelsesnummer</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="105"/>
        <source>Security</source>
        <translation>Sikkerhed</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="106"/>
        <source>Enable SDES key exchange</source>
        <translation>Aktivere SDES- nøgleudveksling</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="107"/>
        <source>Encrypt negotiation (TLS)</source>
        <translation>Kryptér forhandling (TLS)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="108"/>
        <source>CA certificate</source>
        <translation>CA-certifikat</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="109"/>
        <source>User certificate</source>
        <translation>Brugercertifikat</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="110"/>
        <source>Private key</source>
        <translation>Privat nøgle</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="111"/>
        <source>Private key password</source>
        <translation>Adgangskode til privat nøgle</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="112"/>
        <source>Verify certificates for incoming TLS connections</source>
        <translation>Verificere certifikater for indgående TLS-forbindelser</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="113"/>
        <source>Verify server TLS certificates</source>
        <translation>Verificere TLS-certifikater for serveren</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="114"/>
        <source>Require certificate for incoming TLS connections</source>
        <translation>Krav på certifikat for indgående TLS-forbindelser</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="116"/>
        <source>Select a private key</source>
        <translation>Vælg en privat nøgle</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="117"/>
        <source>Select a user certificate</source>
        <translation>Vælg et brugercertifikat</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="118"/>
        <source>Select a CA certificate</source>
        <translation>Vælg CA-certifikat</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="120"/>
        <source>Key File (*.key)</source>
        <translation>Nøglefil (*.nøgle)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="123"/>
        <source>Connectivity</source>
        <translation>Forbindelse</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="124"/>
        <source>Auto Registration After Expired</source>
        <translation>Autorejistrering efter udløbet</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="125"/>
        <source>Registration expiration time (seconds)</source>
        <translation>Registreringstidspunktet for at udløbe (sekunder)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="126"/>
        <source>Network interface</source>
        <translation>Netværksgrænseflade</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="127"/>
        <source>Use UPnP</source>
        <translation>Brug UPnP</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="128"/>
        <source>Use TURN</source>
        <translation>Brug TURN</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="129"/>
        <source>TURN address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="130"/>
        <source>TURN username</source>
        <translation>TURN-brugernavn</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="131"/>
        <source>TURN password</source>
        <translation>TURN-adgangskode</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="132"/>
        <source>TURN Realm</source>
        <translation>TURN-verden</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="133"/>
        <source>Use STUN</source>
        <translation>Brug STUN</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="134"/>
        <source>STUN address</source>
        <translation>STUN-adresse</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="137"/>
        <source>Allow IP Auto Rewrite</source>
        <translation>Tillad IP- automatisk omskrivning</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="138"/>
        <source>Public address</source>
        <translation>Adresse til offentligheden</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="139"/>
        <source>Use custom address and port</source>
        <translation>Brug brugervalgt adresse og port</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="140"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="141"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="144"/>
        <source>Media</source>
        <translation>Medier</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="145"/>
        <source>Enable video</source>
        <translation>Aktivér video</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="153"/>
        <source>SDP Session Negotiation (ICE Fallback)</source>
        <translation>SDP Sessions-forhandling (ICE-tilbagefald)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="154"/>
        <source>Only used during negotiation in case ICE is not supported</source>
        <translation>Kun anvendt under forhandlinger, hvis ICE ikke understøttes</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="155"/>
        <source>Audio RTP minimum Port</source>
        <translation>Audio RTP-minimum Port</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="156"/>
        <source>Audio RTP maximum Port</source>
        <translation>Audio RTP-port</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="157"/>
        <source>Video RTP minimum Port</source>
        <translation>Video RTP-minimum Port</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="158"/>
        <source>Video RTP maximum port</source>
        <translation>Video RTP-port</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="161"/>
        <source>Enable local peer discovery</source>
        <translation>Aktivere lokal peer-opdagelse</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="162"/>
        <source>Connect to other DHT nodes advertising on your local network.</source>
        <translation>Køb en forbindelse til andre DHT-noder, der reklamerer på dit lokale netværk.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="164"/>
        <source>Enable proxy</source>
        <translation>Aktivere proxy</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="165"/>
        <source>Proxy address</source>
        <translation>Proxy-adresse</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="166"/>
        <source>Bootstrap</source>
        <translation>Bootstrap</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="169"/>
        <source>Back</source>
        <translation>Tilbage</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="170"/>
        <source>Account</source>
        <translation>Konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="171"/>
        <source>General</source>
        <translation>Generelt</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="172"/>
        <source>Extensions</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="182"/>
        <source>Audio</source>
        <translation>Lyd</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="183"/>
        <source>Microphone</source>
        <translation>Mikrofon</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="184"/>
        <source>Select audio input device</source>
        <translation>Vælg indgangsanordning</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="185"/>
        <source>Output device</source>
        <translation>Udgangsapparat</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="186"/>
        <source>Select audio output device</source>
        <translation>Vælg lydudgangsanordning</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="187"/>
        <source>Ringtone device</source>
        <translation>Ringtoneapparat</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="188"/>
        <source>Select ringtone output device</source>
        <translation>Vælg ringtone udgangsapparat</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="189"/>
        <source>Audio manager</source>
        <translation>Audio-manager</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="193"/>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="194"/>
        <source>Select video device</source>
        <translation>Vælg videoenhed</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="195"/>
        <source>Device</source>
        <translation>Enhed</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="196"/>
        <source>Resolution</source>
        <translation>Opløsning</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="197"/>
        <source>Select video resolution</source>
        <translation>Vælg videoopløsning</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="198"/>
        <source>Frames per second</source>
        <translation>Råber pr. sekund</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="199"/>
        <source>Select video frame rate (frames per second)</source>
        <translation>Vælg video-ramer (ramer pr. sekund)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="200"/>
        <source>Enable hardware acceleration</source>
        <translation>Aktiver hardwarebesked</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="203"/>
        <source>Select screen sharing frame rate (frames per second)</source>
        <translation>Vælg skærmdeling af billeder (rammer pr. sekund)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="204"/>
        <source>no video</source>
        <translation>ingen video</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="209"/>
        <source>Back up account here</source>
        <translation>- Bygge konto her.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="210"/>
        <source>Back up account</source>
        <translation>Tilbageskrivelse</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="284"/>
        <source>Unavailable</source>
        <translation>Ikke tilgængelig</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="311"/>
        <source>Turn off sharing</source>
        <translation>Sluk for at dele</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="312"/>
        <source>Stop location sharing in this conversation (%1)</source>
        <translation>Stop placeringsholding i denne samtale (%1)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="335"/>
        <source>Hide chat</source>
        <translation>Skjul chat</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="344"/>
        <source>Back to Call</source>
        <translation>Tilbage til opkald</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="353"/>
        <source>Scroll to end of conversation</source>
        <translation>Rul til slutningen af samtalen</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="371"/>
        <source>You can choose a username to help others more easily find and reach you on Jami.</source>
        <translation>Du kan vælge et brugernavn for at hjælpe andre med at finde og nå dig på Jami.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="378"/>
        <source>Are you sure you would like to join Jami without a username?
If yes, only a randomly generated 40-character identifier will be assigned to this account.</source>
        <translation>Hvis ja, vil kun en tilfældigt genereret 40-tegn-identifikator blive tildelt denne konto.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="379"/>
        <source>- 32 characters maximum
- Alphabetical characters (A to Z and a to z)
- Numeric characters (0 to 9)
- Special characters allowed: dash (-)</source>
        <translation>- Maksimalt 32 tegn - Alfabetiske tegn (A til Z og a til z) - Numeriske tegn (0 til 9) - Særlige tegn tilladt: træk (-)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="385"/>
        <source>Your account will be created and stored locally.</source>
        <translation>Din konto vil blive oprettet og gemt lokalt.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="386"/>
        <source>Choosing a username is recommended, and a chosen username CANNOT be changed later.</source>
        <translation>Det anbefales at vælge et brugernavn, og det valgte brugernavn kan IKKE ændres senere.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="387"/>
        <source>Encrypting your account with a password is optional, and if the password is lost it CANNOT be recovered later.</source>
        <translation>Hvis du krypterer din konto med et adgangskode, er det valgfrit, og hvis adgangskoden går tabt, kan den IKKE genvores senere.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="388"/>
        <source>Setting a profile picture and nickname is optional, and can also be changed later in the settings.</source>
        <translation>Indstilling af et profilbillede og et kaldenavn er valgfrit, og kan også ændres senere i indstillingerne.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="397"/>
        <source>TLS</source>
        <translation>TLS</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="398"/>
        <source>UDP</source>
        <translation>UDP</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="399"/>
        <source>Display Name</source>
        <translation>Vis navn</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="403"/>
        <source>A chosen username can help to be found more easily on Jami.
If a username is not chosen, a randomly generated 40-character identifier will be assigned to this account as a username. It is more difficult to be found and reached with this identifier.</source>
        <translation>Hvis et brugernavn ikke er valgt, vil en tilfældigt genereret 40-tegn identifikator blive tildelt denne konto som et brugernavn. Det er sværere at finde og nå frem til med denne identifikator.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="406"/>
        <source>This Jami account exists only on this device.
The account will be lost if this device is lost or the application is uninstalled. It is recommended to make a backup of this account.</source>
        <translation>Denne Jami-konto eksisterer kun på denne enhed. Kontoen vil blive tabt, hvis denne enhed går tabt eller applikationen afinstalleres. Det anbefales at lave en sikkerhedskopiering af denne konto.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="421"/>
        <source>Encrypt account</source>
        <translation>Kryptering af konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="424"/>
        <source>Back up account to a .gz file</source>
        <translation>Baksæt konto til en.gz fil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="430"/>
        <source>This profile is only shared with this account's contacts.
The profile can be changed at all times from the account&apos;s settings.</source>
        <translation>Denne profil deles kun med denne konto&apos;s kontakter. Profilen kan ændres til enhver tid fra kontoens indstillinger.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="431"/>
        <source>Encrypt account with a password</source>
        <translation>Kryptere konto med et adgangskode</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="432"/>
        <source>A Jami account is created and stored locally only on this device, as an archive containing your account keys. Access to this archive can optionally be protected by a password.</source>
        <translation>En Jami-konto oprettes og opbevares lokalt kun på denne enhed, som et arkiv med dine konto nøgler.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="433"/>
        <source>Please note that if you lose your password, it CANNOT be recovered!</source>
        <translation>Bemærk venligst, at hvis du mister dit adgangskode, kan det IKKE genvundres!</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="459"/>
        <source>Would you really like to delete this account?</source>
        <translation>Vil du virkelig slette denne konto?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="460"/>
        <source>If your account has not been backed up or added to another device, your account and registered username will be IRREVOCABLY LOST.</source>
        <translation>Hvis din konto ikke er blevet sikkerhedskopieret eller tilføjet til en anden enhed, vil din konto og registreret brugernavn være IRREVOCABLE LOST.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="471"/>
        <source>Dark</source>
        <translation>Mørke</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="472"/>
        <source>Light</source>
        <translation>Lys</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="480"/>
        <source>Include local video in recording</source>
        <translation>Indtag lokal video i optagelsen</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="481"/>
        <source>Default settings</source>
        <translation>Standardindstillinger</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="484"/>
        <source>Enable typing indicators</source>
        <translation>Aktivere indskrivningsindikatorer</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="485"/>
        <source>Send and receive typing indicators showing that a message is being typed.</source>
        <translation>Send og modtage indskriftsindikatorer, der viser, at der er skrevet en besked.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="486"/>
        <source>Show link preview in conversations</source>
        <translation>Vis linkforsyning i samtaler</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="508"/>
        <source>Delete file from device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="525"/>
        <source>Content access error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="526"/>
        <source>Content not found</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="589"/>
        <source>Enter account password</source>
        <translation>Indtast password til kontoen</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="590"/>
        <source>This account is password encrypted, enter the password to generate a PIN code.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="592"/>
        <source>PIN expired</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="593"/>
        <source>On another device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="594"/>
        <source>Install and launch Jami, select &quot;Import from another device&quot; and scan the QR code.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="595"/>
        <source>Link new device</source>
        <translation>Tilknyt ny enhed</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="596"/>
        <source>In Jami, scan QR code or manually enter the PIN.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="597"/>
        <source>The PIN code is valid for: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="600"/>
        <source>Enter password</source>
        <translation>Indtast adgangskode</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="602"/>
        <source>Enter account password to confirm the removal of this device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="742"/>
        <source>Show less</source>
        <translation>Vis mindre</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="744"/>
        <source>Show preview</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="745"/>
        <source>Continue editing</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="748"/>
        <source>Strikethrough</source>
        <translation>Gennemstregning</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="754"/>
        <source>Unordered list</source>
        <translation>Ikke-ordnet liste</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="755"/>
        <source>Ordered list</source>
        <translation>Ordnet liste</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="758"/>
        <source>Press Shift+Enter to insert a new line</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="759"/>
        <source>Press Enter to insert a new line</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="772"/>
        <source>Select dedicated device for hosting future calls in this swarm. If not set, the host will be the device starting a call.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="773"/>
        <source>Select this device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="774"/>
        <source>Select device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="833"/>
        <source>Appearance</source>
        <translation>Udseende</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="852"/>
        <source>Free and private sharing. &lt;a href=&quot;https://jami.net/donate/&quot;&gt;Donate&lt;/a&gt; to expand it.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="853"/>
        <source>Donate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="854"/>
        <source>If you enjoy using Jami and believe in our mission, would you make a donation?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="855"/>
        <source>Not now</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="856"/>
        <source>Enable donation campaign</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="859"/>
        <source>Enter</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="860"/>
        <source>Shift+Enter</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="861"/>
        <source>Enter or Shift+Enter to insert a new line</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="875"/>
        <source>View</source>
        <translation>Vis</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="862"/>
        <source>Text formatting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="865"/>
        <source>Connected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="866"/>
        <source>Connecting TLS</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="867"/>
        <source>Connecting ICE</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="868"/>
        <source>Connecting</source>
        <translation>Forbinder</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="869"/>
        <source>Waiting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="870"/>
        <source>Contact</source>
        <translation>Kontakt</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="871"/>
        <source>Connection</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="872"/>
        <source>Channels</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="873"/>
        <source>Copy all data</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="874"/>
        <source>Remote: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="496"/>
        <source>Accept transfer limit (in Mb)</source>
        <translation>Antagelse af overførselsgrænser (i Mb)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="516"/>
        <source>A new version of Jami was found
Would you like to update now?</source>
        <translation>Der er fundet en ny version af Jami. Vil du opdatere nu?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="539"/>
        <source>Save recordings to</source>
        <translation>Gem optagelser til</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="540"/>
        <source>Save screenshots to</source>
        <translation>Gem skærmbilleder til</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="582"/>
        <source>Select &quot;Link another device&quot;</source>
        <translation>Vælg &quot;Link en anden enhed&quot;</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="609"/>
        <source>Choose a picture as your avatar</source>
        <translation>Vælg et billede som din avatar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="685"/>
        <source>Share freely and privately with Jami</source>
        <translation>Del frit og privat med Jami</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="711"/>
        <source>Unban</source>
        <translation>Fjern blokering</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="728"/>
        <source>Add</source>
        <translation>Tilføj</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="737"/>
        <source>more emojis</source>
        <translation>mere emoji</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="761"/>
        <source>Reply to</source>
        <translation>Svar på</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="762"/>
        <source>In reply to</source>
        <translation>I svar på</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="763"/>
        <source> replied to</source>
        <translation>svarede på</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="765"/>
        <source>Reply</source>
        <translation>Svar</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="464"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="767"/>
        <source>Edit</source>
        <translation>Rediger</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="768"/>
        <source>Edited</source>
        <translation>Udfærdiget</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="769"/>
        <source>Join call</source>
        <translation>Deltag i opkald</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="770"/>
        <source>A call is in progress. Do you want to join the call?</source>
        <translation>- Vil du være med?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="771"/>
        <source>Current host for this swarm seems unreachable. Do you want to host the call?</source>
        <translation>- Vil du være vært for opkaldet?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="775"/>
        <source>Remove current device</source>
        <translation>Fjern det aktuelle udstyr</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="776"/>
        <source>Host only this call</source>
        <translation>Bare dette opkald</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="777"/>
        <source>Host this call</source>
        <translation>Vær med i opkaldet</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="778"/>
        <source>Make me the default host for future calls</source>
        <translation>Gør mig standardværten til fremtidige opkald</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="793"/>
        <source>Mute conversation</source>
        <translation>Stemme samtaler</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="796"/>
        <source>Default host (calls)</source>
        <translation>Standard host (opkald)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="799"/>
        <source>None</source>
        <translation>Ingen</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="813"/>
        <source>Tip</source>
        <translation>Tip</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="815"/>
        <source>Add a profile picture and nickname to complete your profile</source>
        <translation>Tilføj et profilbillede og et kaldenavn til at udfylde din profil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="36"/>
        <source>Start swarm</source>
        <translation>Start swarm</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="37"/>
        <source>Create swarm</source>
        <translation>Skabe en sværm</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="176"/>
        <source>Call settings</source>
        <translation>Opkaldsindstillinger</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="115"/>
        <source>Disable secure dialog check for incoming TLS data</source>
        <translation>Deaktiver sikker dialogkontrol for indgående TLS-data</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="146"/>
        <source>Video codecs</source>
        <translation>Video-codec&apos;er</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="147"/>
        <source>Audio codecs</source>
        <translation>Lydcodec&apos;er</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="150"/>
        <source>Name server</source>
        <translation>Navneserver</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="163"/>
        <source>OpenDHT configuration</source>
        <translation>OpenDHT-konfiguration</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="201"/>
        <source>Mirror local video</source>
        <translation>Spille lokale videoer</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="207"/>
        <source>Why should I back-up this account?</source>
        <translation>Hvorfor skulle jeg backupe denne konto?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="211"/>
        <source>Success</source>
        <translation>Lykkedes</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="212"/>
        <source>Error</source>
        <translation>Fejl</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="213"/>
        <source>Jami archive files (*.gz)</source>
        <translation>Jami-arkivfiler (*.gz)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="214"/>
        <source>All files (*)</source>
        <translation>Alle filer (*)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="217"/>
        <source>Reinstate as contact</source>
        <translation>Tilbageindføre som kontakt</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="218"/>
        <source>name</source>
        <translation>navn</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="219"/>
        <source>Identifier</source>
        <translation>Identifikator</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="222"/>
        <source>is recording</source>
        <translation>er optagelse</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="223"/>
        <source>are recording</source>
        <translation>er indregneret</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="224"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="716"/>
        <source>Mute</source>
        <translation>Stem</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="225"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="717"/>
        <source>Unmute</source>
        <translation>Uafstemt</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="226"/>
        <source>Pause call</source>
        <translation>Pause-opkald</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="227"/>
        <source>Resume call</source>
        <translation>Opkaldet genoptages</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="228"/>
        <source>Mute camera</source>
        <translation>Stem kamera</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="229"/>
        <source>Unmute camera</source>
        <translation>Udybt kamera</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="230"/>
        <source>Add participant</source>
        <translation>Tilføj deltager</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="231"/>
        <source>Add participants</source>
        <translation>Tilføj deltagere</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="232"/>
        <source>Details</source>
        <translation>Detaljer</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="177"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="233"/>
        <source>Chat</source>
        <translation>Beskeder</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="174"/>
        <source>Manage account</source>
        <translation>Håndter konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="175"/>
        <source>Linked devices</source>
        <translation>Sammenkædede enheder</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="178"/>
        <source>Advanced settings</source>
        <translation>Avancerede indstillinger</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="179"/>
        <source>Audio and Video</source>
        <translation>Audio og video</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="190"/>
        <source>Sound test</source>
        <translation>Lydprøve</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="202"/>
        <source>Screen sharing</source>
        <translation>Skærmdeling</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="208"/>
        <source>Your account only exists on this device. If you lose your device or uninstall the application, your account will be deleted and CANNOT be recovered. You can &lt;a href=&apos;blank&apos;&gt; back up your account &lt;/a&gt; now or later (in the Account Settings).</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="234"/>
        <source>More options</source>
        <translation>Flere muligheder</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="235"/>
        <source>Mosaic</source>
        <translation>Mosaik</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="236"/>
        <source>Participant is still muted on their device</source>
        <translation>Deltageren er stadig stum på deres enhed</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="237"/>
        <source>You are still muted on your device</source>
        <translation>Du er stadig stum på din enhed</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="238"/>
        <source>You are still muted by moderator</source>
        <translation>Du er stadig stillet af moderatoren</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="239"/>
        <source>You are muted by a moderator</source>
        <translation>Du er blevet stillet af en moderator</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="240"/>
        <source>Moderator</source>
        <translation>Moderator</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="241"/>
        <source>Host</source>
        <translation>Værtsværts</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="242"/>
        <source>Local and Moderator muted</source>
        <translation>Lokal og moderator stummet</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="243"/>
        <source>Moderator muted</source>
        <translation>Moderator stummet</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="244"/>
        <source>Not muted</source>
        <translation>Ikke stummet</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="245"/>
        <source>On the side</source>
        <translation>På siden</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="246"/>
        <source>On the top</source>
        <translation>På toppen.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="247"/>
        <source>Hide self</source>
        <translation>Skjul dig selv</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="248"/>
        <source>Hide spectators</source>
        <translation>Skjul tilskuere</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="251"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="554"/>
        <source>Copy</source>
        <translation>Kopiér</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="252"/>
        <source>Share</source>
        <translation>Del</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="253"/>
        <source>Cut</source>
        <translation>Klip</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="254"/>
        <source>Paste</source>
        <translation>Indsæt</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="257"/>
        <source>Start video call</source>
        <translation>Start et video opkald</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="258"/>
        <source>Start audio call</source>
        <translation>Start et audio opkald</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="259"/>
        <source>Clear conversation</source>
        <translation>Ryd samtale</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="260"/>
        <source>Confirm action</source>
        <translation>Bekræftelse af handlingen</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="261"/>
        <source>Remove conversation</source>
        <translation>Fjern samtalen</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="262"/>
        <source>Would you really like to remove this conversation?</source>
        <translation>Vil du virkelig fjerne denne samtale?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="263"/>
        <source>Would you really like to block this conversation?</source>
        <translation>Vil du virkelig blokere denne samtale?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="264"/>
        <source>Remove contact</source>
        <translation>Fjern kontakt</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="265"/>
        <source>Block contact</source>
        <translation>Bloker kontakt</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="266"/>
        <source>Conversation details</source>
        <translation>Samtale detaljer</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="267"/>
        <source>Contact details</source>
        <translation>Kontakt-detajler</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="270"/>
        <source>Sip input panel</source>
        <translation>Indgangspanelet</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="271"/>
        <source>Transfer call</source>
        <translation>Overfør samtale</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="272"/>
        <source>Stop recording</source>
        <translation>Stop optagelse</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="273"/>
        <source>Start recording</source>
        <translation>Start optagelse</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="274"/>
        <source>View full screen</source>
        <translation>Vis fuld skærm</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="275"/>
        <source>Share screen</source>
        <translation>Del skærm</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="276"/>
        <source>Share window</source>
        <translation>Del vindue</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="277"/>
        <source>Stop sharing screen or file</source>
        <translation>Stop med at dele skærm eller fil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="278"/>
        <source>Share screen area</source>
        <translation>Del skærmområde</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="279"/>
        <source>Share file</source>
        <translation>Del fil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="280"/>
        <source>Select sharing method</source>
        <translation>Vælg delingsmetode</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="281"/>
        <source>View plugin</source>
        <translation>Se plugin</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="282"/>
        <source>Advanced information</source>
        <translation>Forfremmede oplysninger</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="283"/>
        <source>No video device</source>
        <translation>Ingen videoenhed</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="285"/>
        <source>Lower hand</source>
        <translation>Nedre hånd</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="286"/>
        <source>Raise hand</source>
        <translation>Løft hånden.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="287"/>
        <source>Layout settings</source>
        <translation>Layout indstillinger</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="288"/>
        <source>Take tile screenshot</source>
        <translation>Tag et skærmbillede af tegler</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="289"/>
        <source>Screenshot saved to %1</source>
        <translation>Skærmbillede gemt til % 1</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="290"/>
        <source>File saved to %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="293"/>
        <source>Renderers information</source>
        <translation>Information til udleverere</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="294"/>
        <source>Call information</source>
        <translation>Opkaldsinformation</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="295"/>
        <source>Peer number</source>
        <translation>Samletal</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="296"/>
        <source>Call id</source>
        <translation>Ring-id</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="297"/>
        <source>Sockets</source>
        <translation>Størrelsesfasen</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="298"/>
        <source>Video codec</source>
        <translation>Video codec</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="299"/>
        <source>Hardware acceleration</source>
        <translation>Hardwarebesked</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="300"/>
        <source>Video bitrate</source>
        <translation>Video bitrate</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="301"/>
        <source>Audio codec</source>
        <translation>Audio-kodek</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="302"/>
        <source>Renderer id</source>
        <translation>Udleverings- og udleverings-id</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="303"/>
        <source>Fps</source>
        <translation>Fps</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="306"/>
        <source>Share location</source>
        <translation>Del lokation</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="307"/>
        <source>Stop sharing</source>
        <translation>Stop deling</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="308"/>
        <source>Your precise location could not be determined.
In Device Settings, please turn on &quot;Location Services&quot;.
Other participants&apos; location can still be received.</source>
        <translation>Det er ikke muligt at bestemme din nøjagtige placering. I Enhedsindstillinger skal du aktivere &quot;Lokationstjenester&quot;. Andre deltagers placering kan stadig modtages.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="309"/>
        <source>Your precise location could not be determined. Please check your Internet connection.</source>
        <translation>Det er ikke muligt at fastslå, hvor du er.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="310"/>
        <source>Turn off location sharing</source>
        <translation>Sluk for placeringsdeling</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="313"/>
        <source>Location is shared in several conversations</source>
        <translation>Lokation er delt i flere samtaler</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="314"/>
        <source>Pin map to be able to share location or to turn off location in specific conversations</source>
        <translation>Pin-kart til at kunne dele placering eller slukke for placering i specifikke samtaler</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="315"/>
        <source>Location is shared in several conversations, click to choose how to turn off location sharing</source>
        <translation>Lokation deles i flere samtaler, klik for at vælge, hvordan man slukker op med at dele lokation</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="316"/>
        <source>Share location to participants of this conversation (%1)</source>
        <translation>Del placering til deltagerne i denne samtale (%1)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="318"/>
        <source>Reduce</source>
        <translation>Reducer</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="319"/>
        <source>Drag</source>
        <translation>Træk</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="320"/>
        <source>Center</source>
        <translation>Centrum</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="322"/>
        <source>Unpin</source>
        <translation>Udskift</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="323"/>
        <source>Pin</source>
        <translation>Pinde</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="324"/>
        <source>Position share duration</source>
        <translation>Varighed af positionaktier</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="325"/>
        <source>Location sharing</source>
        <translation>Lokalisering</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="326"/>
        <source>Unlimited</source>
        <translation>Ubegrænset</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="327"/>
        <source>1 min</source>
        <translation>1 min.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="328"/>
        <source>%1h%2min</source>
        <translation>% 1h% 2min</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="329"/>
        <source>%1h</source>
        <translation>% 1h</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="330"/>
        <source>%1min%2s</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="331"/>
        <source>%1min</source>
        <translation>% 1 min</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="332"/>
        <source>%sec</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="336"/>
        <source>Place audio call</source>
        <translation>Læg et lydopkald</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="337"/>
        <source>Place video call</source>
        <translation>Læg videoopkald</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="338"/>
        <source>Show available plugins</source>
        <translation>Vis tilgængelige plugins</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="339"/>
        <source>Add to conversations</source>
        <translation>Tilføj til samtaler</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="340"/>
        <source>This is the error from the backend: %0</source>
        <translation>Dette er fejlen fra backend: %0</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="341"/>
        <source>The account is disabled</source>
        <translation>Kontoen er deaktiveret</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="342"/>
        <source>No network connectivity</source>
        <translation>Ingen netværksforbindelse</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="343"/>
        <source>Deleted message</source>
        <translation>Slet beskeder</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="347"/>
        <source>Jump to</source>
        <translation>Hop til</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="348"/>
        <source>Messages</source>
        <translation>Beskeder</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="349"/>
        <source>Files</source>
        <translation>Filer</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="350"/>
        <source>Search</source>
        <translation>Søg</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="354"/>
        <source>{} is typing…</source>
        <translation>- Jeg skriver...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="355"/>
        <source>{} are typing…</source>
        <translation>- De skriver...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="356"/>
        <source>Several people are typing…</source>
        <translation>Flere skriver...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="357"/>
        <source> and </source>
        <translation>og </translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="360"/>
        <source>Enter the Jami Account Management Server (JAMS) URL</source>
        <translation>Indtaster Jami Account Management Server (JAMS) URL</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="361"/>
        <source>Jami Account Management Server URL</source>
        <translation>Jami kontoadministration server URL</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="362"/>
        <source>Enter JAMS credentials</source>
        <translation>Indtaster JAMS-oplysninger</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="363"/>
        <source>Connect</source>
        <translation>Køb</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="364"/>
        <source>Creating account…</source>
        <translation>- Jeg opretter en konto.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="367"/>
        <source>Choose name</source>
        <translation>Vælg navn</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="368"/>
        <source>Choose username</source>
        <translation>Vælg brugernavn</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="369"/>
        <source>Choose a username</source>
        <translation>Vælg et brugernavn</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="428"/>
        <source>Encrypt account with password</source>
        <translation>Krypter konto med kodeord</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="370"/>
        <source>Confirm password</source>
        <translation>Bekræft adgangskode</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="372"/>
        <source>Choose a name for your rendezvous point</source>
        <translation>Vælg et navn til dit mødested</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="373"/>
        <source>Choose a name</source>
        <translation>Vælg et navn</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="374"/>
        <source>Invalid name</source>
        <translation>Ugyldigt navn</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="375"/>
        <source>Invalid username</source>
        <translation>Ugyldigt brugernavn</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="376"/>
        <source>Name already taken</source>
        <translation>Navnet er allerede taget</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="377"/>
        <source>Username already taken</source>
        <translation>Brugernavn er allerede taget</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="382"/>
        <source>Good to know</source>
        <translation>Godt at vide.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="383"/>
        <source>Local</source>
        <translation>Lokalt</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="384"/>
        <source>Encrypt</source>
        <translation>Kryptograferet</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="391"/>
        <source>SIP account</source>
        <translation>SIP-konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="392"/>
        <source>Proxy</source>
        <translation>Stedfortræder</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="393"/>
        <source>Server</source>
        <translation>Server</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="394"/>
        <source>Configure an existing SIP account</source>
        <translation>Konfigurere en eksisterende SIP-konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="395"/>
        <source>Personalize account</source>
        <translation>Personliggør konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="396"/>
        <source>Add SIP account</source>
        <translation>Tilføj SIP-konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="402"/>
        <source>Your profile is only shared with your contacts.
Your picture and your nickname can be changed at all time in the settings of your account.</source>
        <translation>Din profil kan kun deles med dine kontakter. Dit billede og dit kaldenavn kan ændres til enhver tid i indstillingerne på din konto.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="404"/>
        <source>Your Jami account is registered only on this device as an archive containing the keys of your account. Access to this archive can be protected by a password.</source>
        <translation>Din Jami-konto er registreret kun på denne enhed som et arkiv, der indeholder nøglerne til din konto.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="405"/>
        <source>Backup account</source>
        <translation>Backupkonto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="407"/>
        <source>Delete your account</source>
        <translation>Slet dit konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="408"/>
        <source>If your account has not been backed up or added to another device, your account and registered name will be irrevocably lost.</source>
        <translation>Hvis din konto ikke er blevet sikkerhedskopieret eller tilføjet til en anden enhed, vil din konto og registreret navn blive uigenkaldeligt tabt.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="409"/>
        <source>List of the devices that are linked to this account:</source>
        <translation>Liste over de enheder, der er knyttet til denne konto:</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="410"/>
        <source>This device</source>
        <translation>Denne enhed</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="411"/>
        <source>Other linked devices</source>
        <translation>Andre tilknyttede enheder</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="414"/>
        <source>Backup successful</source>
        <translation>Backup er lykkedes</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="415"/>
        <source>Backup failed</source>
        <translation>Backup mislykkedes</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="416"/>
        <source>Password changed successfully</source>
        <translation>Passord ændret med succes</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="417"/>
        <source>Password change failed</source>
        <translation>Passordændring mislykkedes</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="418"/>
        <source>Password set successfully</source>
        <translation>Passord indstillet med succes</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="419"/>
        <source>Password set failed</source>
        <translation>Passord indstillet mislykkedes</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="420"/>
        <source>Change password</source>
        <translation>Skift adgangskode</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="434"/>
        <source>Enter a nickname, surname…</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="435"/>
        <source>Use this account on other devices</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="436"/>
        <source>This account is created and stored locally, if you want to use it on another device you have to link the new device to this account.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="465"/>
        <source>Device name:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="550"/>
        <source>Markdown</source>
        <translation>Markdown</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="617"/>
        <source>Auto update</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="618"/>
        <source>Disable all</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="619"/>
        <source>Installed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="620"/>
        <source>Install</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="621"/>
        <source>Installing</source>
        <translation>Installation</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="622"/>
        <source>Install manually</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="623"/>
        <source>Install an extension directly from your device.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="624"/>
        <source>Available</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="625"/>
        <source>Plugins store is not available</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="627"/>
        <source>Installation failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="628"/>
        <source>The installation of the plugin failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="646"/>
        <source>Version %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="647"/>
        <source>Last update %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="648"/>
        <source>By %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="649"/>
        <source>Proposed by %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="652"/>
        <source>More information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="739"/>
        <source>Audio message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="740"/>
        <source>Video message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="741"/>
        <source>Show more</source>
        <translation>Vis mere</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="746"/>
        <source>Bold</source>
        <translation>Fed</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="747"/>
        <source>Italic</source>
        <translation>Kursiv</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="749"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="750"/>
        <source>Heading</source>
        <translation>Overskrift</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="751"/>
        <source>Link</source>
        <translation>Tilknyt</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="752"/>
        <source>Code</source>
        <translation>Kode</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="753"/>
        <source>Quote</source>
        <translation>Citat</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="756"/>
        <source>Show formatting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="757"/>
        <source>Hide formatting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="824"/>
        <source>Share your Jami identifier in order to be contacted more easily!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="825"/>
        <source>Jami identity</source>
        <translation>Jami identitet</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="826"/>
        <source>Show fingerprint</source>
        <translation>Vis fingeraftryk</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="827"/>
        <source>Show registered name</source>
        <translation>Vis registreret navn</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="830"/>
        <source>Enabling your account allows you to be contacted on Jami</source>
        <translation>Hvis du aktiverer din konto, kan du blive kontaktet på Jami.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="836"/>
        <source>Experimental</source>
        <translation>Eksperimental</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="839"/>
        <source>Ringtone</source>
        <translation>Ringetone</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="842"/>
        <source>Rendezvous point</source>
        <translation>Rendezvouspoint</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="845"/>
        <source>Moderation</source>
        <translation>Moderation</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="848"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="849"/>
        <source>Text zoom level</source>
        <translation>Tekst zoomniveau</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="422"/>
        <source>Set a password</source>
        <translation>Indsæt et adgangskode</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="423"/>
        <source>Change current password</source>
        <translation>Ændre det aktuelle adgangskode</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="425"/>
        <source>Display advanced settings</source>
        <translation>Vis avancerede indstillinger</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="426"/>
        <source>Hide advanced settings</source>
        <translation>Skjul avancerede indstillinger</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="173"/>
        <source>Enable account</source>
        <translation>Aktivér konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="427"/>
        <source>Advanced account settings</source>
        <translation>Avancerede kontoindstillinger</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="429"/>
        <source>Customize profile</source>
        <translation>Tilpas profil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="440"/>
        <source>Set username</source>
        <translation>Indstilling af brugernavn</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="441"/>
        <source>Registering name</source>
        <translation>Registreringsnavn</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="444"/>
        <source>Identity</source>
        <translation>Identitet</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="447"/>
        <source>Link a new device to this account</source>
        <translation>Link en ny enhed til denne konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="448"/>
        <source>Exporting account…</source>
        <translation>Eksportkonto...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="449"/>
        <source>Remove Device</source>
        <translation>Fjern enheden</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="450"/>
        <source>Are you sure you wish to remove this device?</source>
        <translation>Er du sikker på, du vil fjerne denne enhed?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="451"/>
        <source>Your PIN is:</source>
        <translation>Din PIN er:</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="452"/>
        <source>Error connecting to the network.
Please try again later.</source>
        <translation>- Det er en fejl i forbindelse med netværket.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="455"/>
        <source>Banned</source>
        <translation>Banned</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="456"/>
        <source>Banned contacts</source>
        <translation>Blokerede kontakter</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="467"/>
        <source>Device Id</source>
        <translation>Enheds-ID</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="470"/>
        <source>System</source>
        <translation>System</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="473"/>
        <source>Select a folder</source>
        <translation>Vælg en mappe</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="474"/>
        <source>Enable notifications</source>
        <translation>Aktiver notifikationer</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="478"/>
        <source>Launch at startup</source>
        <translation>Start ved start</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="479"/>
        <source>Choose download directory</source>
        <translation>Vælg download-mappe</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="487"/>
        <source>Preview requires downloading content from third-party servers.</source>
        <translation>Forhåndsvisning kræver download af indhold fra tredjepartsservere.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="489"/>
        <source>User interface language</source>
        <translation>Brugergrænsefladesprog</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="490"/>
        <source>Vertical view</source>
        <translation>Lodret visning</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="491"/>
        <source>Horizontal view</source>
        <translation>Vandret visning</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="494"/>
        <source>File transfer</source>
        <translation>Filoverførsler</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="495"/>
        <source>Automatically accept incoming files</source>
        <translation>Automatisk acceptere indgående filer</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="497"/>
        <source>in MB, 0 = unlimited</source>
        <translation>i MB, 0 = ubegrænset</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="500"/>
        <source>Register</source>
        <translation>Registrér</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="501"/>
        <source>Incorrect password</source>
        <translation>Forkert adgangskode</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="502"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="519"/>
        <source>Network error</source>
        <translation>Netværksfejl</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="503"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="527"/>
        <source>Something went wrong</source>
        <translation>Der gik noget galt.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="506"/>
        <source>Save file</source>
        <translation>Gem fil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="507"/>
        <source>Open location</source>
        <translation>Åben placering</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="764"/>
        <source>Me</source>
        <translation>Mig</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="511"/>
        <source>Install beta version</source>
        <translation>Installer beta-version</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="512"/>
        <source>Check for updates now</source>
        <translation>Søg efter for opdateringer nu</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="513"/>
        <source>Enable/Disable automatic updates</source>
        <translation>Aktivere/afaktivere automatisk opdatering</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="514"/>
        <source>Updates</source>
        <translation>Opdateringer</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="515"/>
        <source>Update</source>
        <translation>Opdatér</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="517"/>
        <source>No new version of Jami was found</source>
        <translation>Der blev ikke fundet en ny version af Jami.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="518"/>
        <source>An error occured when checking for a new version</source>
        <translation>Der er sket en fejl ved at kontrollere en ny version</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="520"/>
        <source>SSL error</source>
        <translation>SSL-fejl</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="521"/>
        <source>Installer download canceled</source>
        <translation>Download af installationsprogrammet blev annulleret</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="523"/>
        <source>This will uninstall your current Release version and you can always download the latest Release version on our website</source>
        <translation>Dette vil fjerne din nuværende version af Release og du kan altid downloade den nyeste version af Release på vores hjemmeside</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="524"/>
        <source>Network disconnected</source>
        <translation>Netværksafbrydelse</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="530"/>
        <source>Troubleshoot</source>
        <translation>Problemløsning</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="531"/>
        <source>Open logs</source>
        <translation>Åbne logs</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="532"/>
        <source>Get logs</source>
        <translation>Få logs</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="534"/>
        <source>(Experimental) Enable call support for swarm</source>
        <translation>(Experimentel) Aktivere opkaldsstøtte til sværm</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="535"/>
        <source>This feature will enable call buttons in swarms with multiple participants.</source>
        <translation>Denne funktion aktiverer opkaldsknapper i svære med flere deltagere.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="538"/>
        <source>Quality</source>
        <translation>Kvalitet</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="542"/>
        <source>Always record calls</source>
        <translation>- Du kan altid optage opkald.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="545"/>
        <source>Keyboard Shortcut Table</source>
        <translation>Tæbel for tastaturkort</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="546"/>
        <source>Keyboard Shortcuts</source>
        <translation>Tastaturkort</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="547"/>
        <source>Conversation</source>
        <translation>Samtale</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="548"/>
        <source>Call</source>
        <translation>Ring op</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="549"/>
        <source>Settings</source>
        <translation>Indstillinger</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="553"/>
        <source>Debug</source>
        <translation>Debug</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="555"/>
        <source>Report Bug</source>
        <translation>Rapporter bug</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="556"/>
        <source>Clear</source>
        <translation>Ryd</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="557"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="705"/>
        <source>Cancel</source>
        <translation>Annuller</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="558"/>
        <source>Copied to clipboard!</source>
        <translation>Kopereret til klippan!</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="559"/>
        <source>Receive Logs</source>
        <translation>Tag logs</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="562"/>
        <source>Archive</source>
        <translation>Arkiv</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="563"/>
        <source>Open file</source>
        <translation>Åbn fil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="566"/>
        <source>Generating account…</source>
        <translation>Generering af konto...</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="567"/>
        <source>Import from archive backup</source>
        <translation>Import fra backup</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="569"/>
        <source>Select archive file</source>
        <translation>Vælg arkivfil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="437"/>
        <source>Link device</source>
        <translation>Tilknyt enhed</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="573"/>
        <source>Import</source>
        <translation>Importér</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="575"/>
        <source>A PIN is required to use an existing Jami account on this device.</source>
        <translation>En PIN er nødvendig for at bruge en eksisterende Jami-konto på denne enhed.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="581"/>
        <source>Choose the account to link</source>
        <translation>Vælg kontoen til at forbinde</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="587"/>
        <source>The PIN and the account password should be entered in your device within 10 minutes.</source>
        <translation>PIN-numret og kontoenord skal indtasteres i din enhed inden for 10 minutter.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="610"/>
        <source>Choose a picture</source>
        <translation>Vælg et billede</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="790"/>
        <source>Contact&apos;s name</source>
        <translation>Kontaktpersonens navn</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="805"/>
        <source>Reinstate member</source>
        <translation>Tilbageindsat medlem</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="819"/>
        <source>Delete message</source>
        <translation>Slet besked</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="820"/>
        <source>*(Deleted Message)*</source>
        <translation>*(Begrænset besked) *</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="821"/>
        <source>Edit message</source>
        <translation>Redigere beskeden</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="321"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="588"/>
        <source>Close</source>
        <translation>Luk</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="541"/>
        <source>Call recording</source>
        <translation>Optagelse af opkald</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="570"/>
        <source>If the account is encrypted with a password, please fill the following field.</source>
        <translation>Hvis kontoen er krypteret med et adgangskode, bedes du udfylde følgende felt.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="574"/>
        <source>Enter the PIN code</source>
        <translation>Indtast PIN-koden</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="576"/>
        <source>Step 01</source>
        <translation>Trin 1</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="577"/>
        <source>Step 02</source>
        <translation>Trin 2</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="578"/>
        <source>Step 03</source>
        <translation>Trin 3</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="579"/>
        <source>Step 04</source>
        <translation>Trin 4</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="580"/>
        <source>Go to the account management settings of a previous device</source>
        <translation>Gå til kontoadministrationsindstillingerne på en tidligere enhed</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="583"/>
        <source>The PIN code will be available for 10 minutes</source>
        <translation>PIN-koden er tilgængelig i 10 minutter.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="584"/>
        <source>Fill if the account is password-encrypted.</source>
        <translation>Udfyld, hvis kontoen er krypteret med adgangskode.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="591"/>
        <source>Add Device</source>
        <translation>Tilføj enhed</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="601"/>
        <source>Enter current password</source>
        <translation>Indtaster det aktuelle adgangskode</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="603"/>
        <source>Enter new password</source>
        <translation>Indtaster nyt adgangskode</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="604"/>
        <source>Confirm new password</source>
        <translation>bekræft nyt adgangskode</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="605"/>
        <source>Change</source>
        <translation>Ændring</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="606"/>
        <source>Export</source>
        <translation>Eksport</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="611"/>
        <source>Import avatar from image file</source>
        <translation>Import avatar fra billedfil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="612"/>
        <source>Clear avatar image</source>
        <translation>Klar avatar billede</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="613"/>
        <source>Take photo</source>
        <translation>Tag billede</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="626"/>
        <source>Preferences</source>
        <translation>Fortrinsret</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="629"/>
        <source>Reset</source>
        <translation>Omstilling</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="630"/>
        <source>Uninstall</source>
        <translation>Afinstallere</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="631"/>
        <source>Reset Preferences</source>
        <translation>Omstilling af præferencer</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="632"/>
        <source>Select a plugin to install</source>
        <translation>Vælg et plugin til at installere</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="633"/>
        <source>Uninstall plugin</source>
        <translation>Afinstallere plugin</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="634"/>
        <source>Are you sure you wish to reset %1 preferences?</source>
        <translation>Er du sikker på, at du ønsker at genindsætte %1 præferencer?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="635"/>
        <source>Are you sure you wish to uninstall %1?</source>
        <translation>Er du sikker på, at du vil fjerne %1?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="636"/>
        <source>Go back to plugins list</source>
        <translation>Gå tilbage til plugins liste</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="637"/>
        <source>Select a file</source>
        <translation>Vælg en fil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="119"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="638"/>
        <source>Select</source>
        <translation>Vælg</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="639"/>
        <source>Choose image file</source>
        <translation>Vælg billedfil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="640"/>
        <source>Plugin Files (*.jpl)</source>
        <translation>Plugin-filer (*.jpl)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="641"/>
        <source>Load/Unload</source>
        <translation>Lade/udlade</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="642"/>
        <source>Select An Image to %1</source>
        <translation>Vælg et billede til % 1</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="643"/>
        <source>Edit preference</source>
        <translation>Redigere præferencer</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="644"/>
        <source>On/Off</source>
        <translation>Ud / på</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="645"/>
        <source>Choose Plugin</source>
        <translation>Vælg Plugin</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="651"/>
        <source>Information</source>
        <translation>Oplysninger</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="653"/>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="656"/>
        <source>Enter the account password to confirm the removal of this device</source>
        <translation>Indtaster konto adgangskoden for at bekræfte fjernelse af dette udstyr</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="659"/>
        <source>Select a screen to share</source>
        <translation>Vælg en skærm at dele</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="660"/>
        <source>Select a window to share</source>
        <translation>Vælg et vindue, du vil dele</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="661"/>
        <source>All Screens</source>
        <translation>Alle skærme</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="662"/>
        <source>Screens</source>
        <translation>Skærme</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="663"/>
        <source>Windows</source>
        <translation>Vinduer</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="664"/>
        <source>Screen %1</source>
        <translation>Skærm % 1</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="667"/>
        <source>QR code</source>
        <translation>QR-kode</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="670"/>
        <source>Link this device to an existing account</source>
        <translation>Tilnyt denne enhed til en eksisterende konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="671"/>
        <source>Import from another device</source>
        <translation>Import fra et andet udstyr</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="672"/>
        <source>Import from an archive backup</source>
        <translation>Import fra et backup</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="673"/>
        <source>Advanced features</source>
        <translation>Avancerede funktioner</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="674"/>
        <source>Show advanced features</source>
        <translation>Vis avancerede funktioner</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="675"/>
        <source>Hide advanced features</source>
        <translation>Skjul avancerede funktioner</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="676"/>
        <source>Connect to a JAMS server</source>
        <translation>Forbindelse til en JAMS-server</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="677"/>
        <source>Create account from Jami Account Management Server (JAMS)</source>
        <translation>Oprette en konto fra Jami Account Management Server (JAMS)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="678"/>
        <source>Configure a SIP account</source>
        <translation>Konfigurere en SIP-konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="679"/>
        <source>Error while creating your account. Check your credentials.</source>
        <translation>Fejl under oprettelsen af din konto.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="680"/>
        <source>Create a rendezvous point</source>
        <translation>Skaf et mødested</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="681"/>
        <source>Join Jami</source>
        <translation>Gå med Jami</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="682"/>
        <source>Create new Jami account</source>
        <translation>Oprette et nyt Jami-konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="683"/>
        <source>Create new SIP account</source>
        <translation>Oprette et nyt SIP-konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="684"/>
        <source>About Jami</source>
        <translation>Om Jami</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="686"/>
        <source>I already have an account</source>
        <translation>Jeg har allerede en konto.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="687"/>
        <source>Use existing Jami account</source>
        <translation>Brug eksisterende Jami-konto</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="688"/>
        <source>Welcome to Jami</source>
        <translation>Velkommen til Jami</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="691"/>
        <source>Clear Text</source>
        <translation>Klar tekst</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="692"/>
        <source>Conversations</source>
        <translation>Samtaler</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="693"/>
        <source>Search Results</source>
        <translation>Søgeresultater</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="696"/>
        <source>Decline contact request</source>
        <translation>Afvis kontaktansøgning</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="697"/>
        <source>Accept contact request</source>
        <translation>Antagelse af kontaktansøgning</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="700"/>
        <source>Automatically check for updates</source>
        <translation>Søg automatisk efter opdateringer</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="703"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="463"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="704"/>
        <source>Save</source>
        <translation>Gem</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="706"/>
        <source>Upgrade</source>
        <translation>Opgradering</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="707"/>
        <source>Later</source>
        <translation>Senere</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="708"/>
        <source>Delete</source>
        <translation>Slet</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="710"/>
        <source>Block</source>
        <translation>Blokér</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="714"/>
        <source>Set moderator</source>
        <translation>Indstilling af moderator</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="715"/>
        <source>Unset moderator</source>
        <translation>Moderator</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="317"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="718"/>
        <source>Maximize</source>
        <translation>Maksimer</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="719"/>
        <source>Minimize</source>
        <translation>Minimer</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="720"/>
        <source>Hangup</source>
        <translation>Læg på</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="721"/>
        <source>Local muted</source>
        <translation>Lokalt stummet</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="724"/>
        <source>Default moderators</source>
        <translation>Standardmoderatorer</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="725"/>
        <source>Enable local moderators</source>
        <translation>Aktivere lokale moderatorer</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="726"/>
        <source>Make all participants moderators</source>
        <translation>Alle deltagere skal være moderatorer</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="727"/>
        <source>Add default moderator</source>
        <translation>Tilføj standardmoderator</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="729"/>
        <source>Remove default moderator</source>
        <translation>Fjern standardmoderator</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="736"/>
        <source>Add emoji</source>
        <translation>Tilføj emoji</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="738"/>
        <source>Send file</source>
        <translation>Send fil</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="760"/>
        <source>Send</source>
        <translation>Send</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="466"/>
        <location filename="../src/app/constant/JamiStrings.qml" line="709"/>
        <source>Remove</source>
        <translation>Fjern</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="41"/>
        <source>Migrate conversation</source>
        <translation>Migrere samtale</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="475"/>
        <source>Show notifications</source>
        <translation>Vis meddelelser</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="476"/>
        <source>Minimize on close</source>
        <translation>Minimer på tæt</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="477"/>
        <source>Run at system startup</source>
        <translation>Løb ved systemstart</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="564"/>
        <source>Create account from backup</source>
        <translation>Oprette konto fra backup</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="565"/>
        <source>Restore account from backup</source>
        <translation>Genopretning af kontoen fra backup</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="568"/>
        <source>Import Jami account from local archive file.</source>
        <translation>Import Jami-konto fra lokale arkiv.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="614"/>
        <source>Image Files (*.png *.jpg *.jpeg *.JPG *.JPEG *.PNG)</source>
        <translation>Billedfiler (*.png *.jpg *.jpeg *.JPG *.JPEG *.PNG)</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="766"/>
        <source>Write to %1</source>
        <translation>Skriv til %1</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="781"/>
        <source>%1 has sent you a request for a conversation.</source>
        <translation>%1 har sendt dig en anmodning om en samtale.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="782"/>
        <source>Hello,
Would you like to join the conversation?</source>
        <translation>Vil du være med i samtalen?</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="783"/>
        <source>You have accepted
the conversation request</source>
        <translation>Du har accepteret forespørgslen om samtale.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="784"/>
        <source>Waiting until %1
connects to synchronize the conversation.</source>
        <translation>Vent til %1 forbinder til at synkronisere samtalen.</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="787"/>
        <source>%1 Members</source>
        <translation>%1 Medlemmer</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="788"/>
        <source>Member</source>
        <translation>Medlem</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="789"/>
        <source>Swarm&apos;s name</source>
        <translation>Swarms navn</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="791"/>
        <source>Add a description</source>
        <translation>Tilføj en beskrivelse</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="794"/>
        <source>Ignore all notifications from this conversation</source>
        <translation>Ignorer alle meddelelser fra denne samtale</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="795"/>
        <source>Choose a color</source>
        <translation>Vælg en farve</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="797"/>
        <source>Leave conversation</source>
        <translation>Lad samtalen være</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="798"/>
        <source>Type of swarm</source>
        <translation>Type svam</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="802"/>
        <source>Create the swarm</source>
        <translation>Skaf sværmen</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="803"/>
        <source>Go to conversation</source>
        <translation>Gå til samtale</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="804"/>
        <source>Kick member</source>
        <translation>Kicke medlem</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="806"/>
        <source>Administrator</source>
        <translation>Administrator</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="807"/>
        <source>Invited</source>
        <translation>Indbydes</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="808"/>
        <source>Remove member</source>
        <translation>Fjern medlem</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="809"/>
        <source>To:</source>
        <translation>Til:</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="812"/>
        <source>Customize</source>
        <translation>Tilpasning</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="814"/>
        <source>Dismiss</source>
        <translation>Afvis</translation>
    </message>
    <message>
        <location filename="../src/app/constant/JamiStrings.qml" line="816"/>
        <source>Your profile is only shared with your contacts</source>
        <translation>Din profil vil kun blive delt med dine kontakter</translation>
    </message>
</context>
<context>
    <name>KeyboardShortcutTable</name>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="41"/>
        <source>Open account list</source>
        <translation>Oven konto liste</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="45"/>
        <source>Focus conversations list</source>
        <translation>Fokus-samtaler liste</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="49"/>
        <source>Requests list</source>
        <translation>Liste over anmodninger</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="53"/>
        <source>Previous conversation</source>
        <translation>Tidligere samtale</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="57"/>
        <source>Next conversation</source>
        <translation>Næste samtale</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="61"/>
        <source>Search bar</source>
        <translation>Søgbar</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="65"/>
        <source>Full screen</source>
        <translation>Fuld skærm</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="69"/>
        <source>Increase font size</source>
        <translation>Forøg tegnsprogstørrelsen</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="73"/>
        <source>Decrease font size</source>
        <translation>Reducere skrifttypen</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="77"/>
        <source>Reset font size</source>
        <translation>Omstilling af skrifttype</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="85"/>
        <source>Start an audio call</source>
        <translation>Start et opkald</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="89"/>
        <source>Start a video call</source>
        <translation>Start et videoopkald</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="93"/>
        <source>Clear history</source>
        <translation>Ryd historiken</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="97"/>
        <source>Search messages/files</source>
        <translation>Søgningsmeddelelser/filer</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="101"/>
        <source>Block contact</source>
        <translation>Bloker kontakt</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="105"/>
        <source>Remove conversation</source>
        <translation>Fjern samtalen</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="109"/>
        <source>Accept contact request</source>
        <translation>Antagelse af kontaktansøgning</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="113"/>
        <source>Edit last message</source>
        <translation>Redigere sidste besked</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="117"/>
        <source>Cancel message edition</source>
        <translation>Afbestilling af meddelelsen</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="125"/>
        <source>Media settings</source>
        <translation>Indstillinger for medier</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="129"/>
        <source>General settings</source>
        <translation>Generelle indstillinger</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="133"/>
        <source>Account settings</source>
        <translation>Kontoindstillinger</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="137"/>
        <source>Plugin settings</source>
        <translation>Pluginindstillinger</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="141"/>
        <source>Open account creation wizard</source>
        <translation>Åben konto skabelsesværktøj</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="146"/>
        <source>Open keyboard shortcut table</source>
        <translation>Åben tastaturkortbord</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="154"/>
        <source>Answer an incoming call</source>
        <translation>Svar på et indgående opkald</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="158"/>
        <source>End call</source>
        <translation>Afslut opkald</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="162"/>
        <source>Decline the call request</source>
        <translation>Afvises opkaldsansøgningen</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="166"/>
        <source>Mute microphone</source>
        <translation>Sluk mikrofon</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="170"/>
        <source>Stop camera</source>
        <translation>Stop kameraet</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="174"/>
        <source>Take tile screenshot</source>
        <translation>Tag et skærmbillede af tegler</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="182"/>
        <source>Bold</source>
        <translation>Fed</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="186"/>
        <source>Italic</source>
        <translation>Kursiv</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="190"/>
        <source>Strikethrough</source>
        <translation>Gennemstregning</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="194"/>
        <source>Heading</source>
        <translation>Overskrift</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="198"/>
        <source>Link</source>
        <translation>Tilknyt</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="202"/>
        <source>Code</source>
        <translation>Kode</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="206"/>
        <source>Quote</source>
        <translation>Citat</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="210"/>
        <source>Unordered list</source>
        <translation>Ikke-ordnet liste</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="214"/>
        <source>Ordered list</source>
        <translation>Ordnet liste</translation>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="218"/>
        <source>Show formatting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/mainview/components/KeyboardShortcutTable.qml" line="222"/>
        <source>Show preview</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>MainApplication</name>
    <message>
        <location filename="../src/app/mainapplication.cpp" line="375"/>
        <source>E&amp;xit</source>
        <translation>E&amp;xit</translation>
    </message>
    <message>
        <location filename="../src/app/mainapplication.cpp" line="377"/>
        <source>&amp;Quit</source>
        <translation>&amp;Kontinuer</translation>
    </message>
    <message>
        <location filename="../src/app/mainapplication.cpp" line="383"/>
        <source>&amp;Show Jami</source>
        <translation>&amp;Show Jami</translation>
    </message>
</context>
<context>
    <name>PositionManager</name>
    <message>
        <location filename="../src/app/positionmanager.cpp" line="414"/>
        <source>%1 is sharing their location</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/positionmanager.cpp" line="419"/>
        <source>Location sharing</source>
        <translation>Lokalisering</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/libclient/qtwrapper/callmanager_wrap.h" line="458"/>
        <source>Me</source>
        <translation>Mig</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="62"/>
        <source>Hold</source>
        <translation>Parker</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="64"/>
        <source>Talking</source>
        <translation>Taler</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="66"/>
        <source>ERROR</source>
        <translation>FEJL</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="68"/>
        <source>Incoming</source>
        <translation>Indgående</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="70"/>
        <source>Calling</source>
        <translation>Ringer op</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="373"/>
        <location filename="../src/libclient/api/call.h" line="72"/>
        <source>Connecting</source>
        <translation>Forbinder</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="74"/>
        <source>Searching</source>
        <translation>Søger</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="76"/>
        <source>Inactive</source>
        <translation>Inaktiv</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="390"/>
        <location filename="../src/libclient/api/call.h" line="78"/>
        <location filename="../src/libclient/api/call.h" line="84"/>
        <source>Finished</source>
        <translation>Afsluttet</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="80"/>
        <source>Timeout</source>
        <translation>Timeout</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="82"/>
        <source>Peer busy</source>
        <translation>Partner optaget</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/call.h" line="86"/>
        <source>Communication established</source>
        <translation>Kommunikation etableret</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="211"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="994"/>
        <source>Invitation received</source>
        <translation>Invitation modtaget</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="260"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="208"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="992"/>
        <source>Contact added</source>
        <translation>Kontakt tilføjet</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="262"/>
        <source>%1 was invited to join</source>
        <translation>%1 blev inviteret til at deltage</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="264"/>
        <source>%1 joined</source>
        <translation>%1 deltager</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="266"/>
        <source>%1 left</source>
        <translation>% 1 venstre</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="268"/>
        <source>%1 was kicked</source>
        <translation>%1 blev sparket</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="270"/>
        <source>%1 was re-added</source>
        <translation>%1 blev tilføjet igen</translation>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="359"/>
        <source>Private conversation created</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/libclient/api/interaction.h" line="361"/>
        <source>Swarm created</source>
        <translation>Skarm skabt</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="174"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="180"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="990"/>
        <source>Outgoing call</source>
        <translation>Udgående opkald</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="176"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="186"/>
        <source>Incoming call</source>
        <translation>Indgående opkald</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="182"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="988"/>
        <source>Missed outgoing call</source>
        <translation>Ubesvarede udgående opkald</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="188"/>
        <source>Missed incoming call</source>
        <translation>Ubesvarede indgående opkald</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="198"/>
        <source>Join call</source>
        <translation>Deltag i opkald</translation>
    </message>
    <message>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="213"/>
        <location filename="../src/libclient/authority/storagehelper.cpp" line="996"/>
        <source>Invitation accepted</source>
        <translation>Invitation accepteret</translation>
    </message>
    <message>
        <location filename="../src/libclient/avmodel.cpp" line="391"/>
        <location filename="../src/libclient/avmodel.cpp" line="410"/>
        <source>default</source>
        <translation>standard</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="72"/>
        <source>Null</source>
        <translation>Null</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="73"/>
        <source>Trying</source>
        <translation>Jeg prøver.</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="74"/>
        <source>Ringing</source>
        <translation>Ringer</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="75"/>
        <source>Being Forwarded</source>
        <translation>At blive sendt videre</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="76"/>
        <source>Queued</source>
        <translation>I kø</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="77"/>
        <source>Progress</source>
        <translation>Fremskridt</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="78"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="79"/>
        <source>Accepted</source>
        <translation>Akcepteret</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="80"/>
        <source>Multiple Choices</source>
        <translation>Flere valgmuligheder</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="81"/>
        <source>Moved Permanently</source>
        <translation>Bevægelse permanent</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="82"/>
        <source>Moved Temporarily</source>
        <translation>Tidenligt flyttet</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="83"/>
        <source>Use Proxy</source>
        <translation>Brug proxy</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="84"/>
        <source>Alternative Service</source>
        <translation>Alternativt service</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="85"/>
        <source>Bad Request</source>
        <translation>En dårlig anmodning</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="86"/>
        <source>Unauthorized</source>
        <translation>Utilladet</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="87"/>
        <source>Payment Required</source>
        <translation>Betaling påkrævet</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="88"/>
        <source>Forbidden</source>
        <translation>Forbudt</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="89"/>
        <source>Not Found</source>
        <translation>Ikke fundet</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="90"/>
        <source>Method Not Allowed</source>
        <translation>Metode ikke tilladt</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="91"/>
        <location filename="../src/libclient/callmodel.cpp" line="111"/>
        <source>Not Acceptable</source>
        <translation>Ikke acceptabelt</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="92"/>
        <source>Proxy Authentication Required</source>
        <translation>Der kræves en fuldmagtsverifikation</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="93"/>
        <source>Request Timeout</source>
        <translation>Anmodning fik timeout</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="94"/>
        <source>Gone</source>
        <translation>- Jeg er væk.</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="95"/>
        <source>Request Entity Too Large</source>
        <translation>Anmodningsenhed for stor</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="96"/>
        <source>Request URI Too Long</source>
        <translation>Anmodning om URI for lang tid</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="97"/>
        <source>Unsupported Media Type</source>
        <translation>Ustøttede medietyper</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="98"/>
        <source>Unsupported URI Scheme</source>
        <translation>Udsupportet URI-ordning</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="99"/>
        <source>Bad Extension</source>
        <translation>Uheldig udvidelse</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="100"/>
        <source>Extension Required</source>
        <translation>Udvidelse påkrævet</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="101"/>
        <source>Session Timer Too Small</source>
        <translation>Sessionstimeret er for lille</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="102"/>
        <source>Interval Too Brief</source>
        <translation>Intervallet er for kort</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="103"/>
        <source>Temporarily Unavailable</source>
        <translation>Tilgængelig midlertidigt</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="104"/>
        <source>Call TSX Does Not Exist</source>
        <translation>Opkald TSX Eksisterer ikke</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="105"/>
        <source>Loop Detected</source>
        <translation>Løb opdaget</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="106"/>
        <source>Too Many Hops</source>
        <translation>For mange hopper</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="107"/>
        <source>Address Incomplete</source>
        <translation>Adresse Fuldstændig</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="108"/>
        <source>Ambiguous</source>
        <translation>Tvetydigt</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="109"/>
        <source>Busy</source>
        <translation>Optaget</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="110"/>
        <source>Request Terminated</source>
        <translation>Anmodning afsløret</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="112"/>
        <source>Bad Event</source>
        <translation>En dårlig begivenhed</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="113"/>
        <source>Request Updated</source>
        <translation>Anmodning opdateret</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="114"/>
        <source>Request Pending</source>
        <translation>Anmodning i vente</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="115"/>
        <source>Undecipherable</source>
        <translation>Uoverskuelig</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="116"/>
        <source>Internal Server Error</source>
        <translation>Intern serverfejl</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="117"/>
        <source>Not Implemented</source>
        <translation>Ikke gennemført</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="118"/>
        <source>Bad Gateway</source>
        <translation>Syg gateway</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="119"/>
        <source>Service Unavailable</source>
        <translation>Tjeneste ikke tilgængelig</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="120"/>
        <source>Server Timeout</source>
        <translation>Server Timeout</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="121"/>
        <source>Version Not Supported</source>
        <translation>Version Ikke understøttet</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="122"/>
        <source>Message Too Large</source>
        <translation>Budskabet er for stort</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="123"/>
        <source>Precondition Failure</source>
        <translation>Forudsætningsfejl</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="124"/>
        <source>Busy Everywhere</source>
        <translation>Besat overalt</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="125"/>
        <source>Call Refused</source>
        <translation>Opkald afvist</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="126"/>
        <source>Does Not Exist Anywhere</source>
        <translation>Der findes ingen steder</translation>
    </message>
    <message>
        <location filename="../src/libclient/callmodel.cpp" line="127"/>
        <source>Not Acceptable Anywhere</source>
        <translation>Ikke acceptabelt overalt</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="375"/>
        <source>Accept</source>
        <translation>Accepter</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="367"/>
        <source>Sending</source>
        <translation>Sende</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="371"/>
        <source>Sent</source>
        <translation>Sendt</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="380"/>
        <source>Unable to make contact</source>
        <translation>Kan ikke kontakte</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="384"/>
        <source>Waiting for contact</source>
        <translation>Ventende på kontakt</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="386"/>
        <source>Incoming transfer</source>
        <translation>Indgående overførsel</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="388"/>
        <source>Timed out waiting for contact</source>
        <translation>Tider ud ventet på kontakt</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="742"/>
        <source>Today</source>
        <translation>I dag</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="744"/>
        <source>Yesterday</source>
        <translation>I går</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="377"/>
        <source>Canceled</source>
        <translation>Annulleret</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="382"/>
        <source>Ongoing</source>
        <translation>Forløbende</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="723"/>
        <source>just now</source>
        <translation>- Jeg er ikke klar.</translation>
    </message>
    <message>
        <location filename="../src/app/messagesadapter.cpp" line="369"/>
        <source>Failure</source>
        <translation>Uden at gennemføre</translation>
    </message>
    <message>
        <location filename="../src/app/positioning.cpp" line="109"/>
        <source>locationServicesError</source>
        <translation>LokationServicesFejl</translation>
    </message>
    <message>
        <location filename="../src/app/positioning.cpp" line="112"/>
        <source>locationServicesClosedError</source>
        <translation>LokationServicesFuldførtFejl</translation>
    </message>
    <message>
        <location filename="../src/app/positioning.cpp" line="114"/>
        <source>locationServicesUnknownError</source>
        <translation>LokationServicesUkendtFejl</translation>
    </message>
    <message>
        <location filename="../src/libclient/conversationmodel.cpp" line="1166"/>
        <location filename="../src/libclient/conversationmodel.cpp" line="1179"/>
        <source>%1 (you)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SmartListModel</name>
    <message>
        <location filename="../src/app/smartlistmodel.cpp" line="68"/>
        <location filename="../src/app/smartlistmodel.cpp" line="107"/>
        <location filename="../src/app/smartlistmodel.cpp" line="115"/>
        <location filename="../src/app/smartlistmodel.cpp" line="161"/>
        <location filename="../src/app/smartlistmodel.cpp" line="191"/>
        <location filename="../src/app/smartlistmodel.cpp" line="192"/>
        <source>Calls</source>
        <translation>Opkald</translation>
    </message>
    <message>
        <location filename="../src/app/smartlistmodel.cpp" line="69"/>
        <location filename="../src/app/smartlistmodel.cpp" line="108"/>
        <location filename="../src/app/smartlistmodel.cpp" line="125"/>
        <location filename="../src/app/smartlistmodel.cpp" line="162"/>
        <location filename="../src/app/smartlistmodel.cpp" line="193"/>
        <location filename="../src/app/smartlistmodel.cpp" line="194"/>
        <source>Contacts</source>
        <translation>Kontakter</translation>
    </message>
</context>
<context>
    <name>SystemTray</name>
    <message>
        <location filename="../src/app/systemtray.cpp" line="216"/>
        <source>Answer</source>
        <translation>Besvar</translation>
    </message>
    <message>
        <location filename="../src/app/systemtray.cpp" line="217"/>
        <source>Decline</source>
        <translation>Afvis</translation>
    </message>
    <message>
        <location filename="../src/app/systemtray.cpp" line="219"/>
        <source>Open conversation</source>
        <translation>Åbn samtale</translation>
    </message>
    <message>
        <location filename="../src/app/systemtray.cpp" line="221"/>
        <source>Accept</source>
        <translation>Accepter</translation>
    </message>
    <message>
        <location filename="../src/app/systemtray.cpp" line="222"/>
        <source>Refuse</source>
        <translation>Afvis</translation>
    </message>
</context>
<context>
    <name>TipsModel</name>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="85"/>
        <source>Customize</source>
        <translation>Tilpasning</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="88"/>
        <source>What does Jami mean?</source>
        <translation>Hvad betyder Jami?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="94"/>
        <source>What is the green dot next to my account?</source>
        <translation>Hvad er den grønne prik ved siden af min konto?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="90"/>
        <source>The choice of the name Jami was inspired by the Swahili word &apos;jamii&apos;, which means &apos;community&apos; as a noun and &apos;together&apos; as an adverb.</source>
        <translation>Valget af navnet Jami blev inspireret af swahili ordet &apos;jamii&apos;, som betyder&apos;samfund&apos; som et substantiv og&apos;sammen&apos; som et adverb.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="81"/>
        <source>Donate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="86"/>
        <source>Backup account</source>
        <translation>Backupkonto</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="96"/>
        <source>A red dot means that your account is disconnected from the network; it turns green when it&apos;s connected.</source>
        <translation>En rød prik betyder, at din konto er afskåret fra netværket; den bliver grøn, når den er forbundet.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="101"/>
        <source>Why should I back up my account?</source>
        <translation>Hvorfor skulle jeg backupe min konto?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="103"/>
        <source>Jami is distributed and your account is only stored locally on your device. If you lose your password or your local account data, you WILL NOT be able to recover your account if you did not back it up earlier.</source>
        <translation>Jami distribueres, og din konto gemmes kun lokalt på din enhed. Hvis du mister dit adgangskode eller dine lokale kontooplysninger, vil du IKKE kunne gendanne din konto, hvis du ikke har lavet en sikkerhedskopi af den tidligere.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="109"/>
        <source>Can I make a conference call?</source>
        <translation>Må jeg ringe til en konference?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="114"/>
        <source>What is a Jami account?</source>
        <translation>Hvad er en Jami-konto?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="116"/>
        <source>A Jami account is an asymmetric encryption key. Your account is identified by a Jami ID, which is a fingerprint of your public key.</source>
        <translation>En Jami-konto er en asymmetrisk krypteringsnøgle. Din konto identificeres ved en Jami-ID, som er et fingeraftryk af din offentlige nøgle.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="120"/>
        <source>What information do I need to provide to create a Jami account?</source>
        <translation>Hvilke oplysninger skal jeg give for at oprette en Jami-konto?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="122"/>
        <source>When you create a new Jami account, you do not have to provide any private information like an email, address, or phone number.</source>
        <translation>Når du opretter en ny Jami-konto, behøver du ikke at give nogen personlige oplysninger som en e-mail, adresse eller telefonnummer.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="129"/>
        <source>With Jami, your account is stored in a directory on your device. The password is only used to encrypt your account in order to protect you from someone who has physical access to your device.</source>
        <translation>Hvis du bruger en kode, kan du bruge din konto til at beskytte dig mod nogen, der har fysisk adgang til din enhed.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="149"/>
        <source>Your account is only stored on your own devices. If you delete your account from all of your devices, the account is gone forever and you CANNOT recover it.</source>
        <translation>Hvis du sletter din konto fra alle dine enheder, er kontoen væk for evigt, og du kan IKKE gendanne den.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="153"/>
        <source>Can I use my account on multiple devices?</source>
        <translation>Kan jeg bruge min konto på flere enheder?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="155"/>
        <source>Yes, you can link your account from the settings, or you can import your backup on another device.</source>
        <translation>Ja, du kan linkere din konto fra indstillingerne, eller du kan importere din backup på en anden enhed.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="127"/>
        <source>Why don&apos;t I have to use a password?</source>
        <translation>Hvorfor må jeg ikke bruge et passord?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="111"/>
        <source>In a call, you can click on &quot;Add participants&quot; to add a contact to a call.</source>
        <translation>I en opkald kan du klikke på &quot;Tilføj deltagere&quot; for at tilføje en kontakt til en opkald.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="135"/>
        <source>Why don&apos;t I have to register a username?</source>
        <translation>Hvorfor skal jeg ikke registrere et brugernavn?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="137"/>
        <source>The most permanent, secure identifier is your Jami ID, but since these are difficult to use for some people, you also have the option of registering a username.</source>
        <translation>Den mest permanente og sikre identifikator er dit Jami-ID, men da disse er vanskelige at bruge for nogle mennesker, har du også mulighed for at registrere et brugernavn.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="142"/>
        <source>How can I back up my account?</source>
        <translation>Hvordan kan jeg sikkerhedskopiere min konto?</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="143"/>
        <source>In Account Settings, a button is available to create a backup your account.</source>
        <translation>I kontoindstillinger er der en knap til rådighed til at oprette en backup af din konto.</translation>
    </message>
    <message>
        <location filename="../src/app/tipsmodel.cpp" line="147"/>
        <source>What happens when I delete my account?</source>
        <translation>Hvad sker der, når jeg sletter min konto?</translation>
    </message>
</context>
<context>
    <name>UtilsAdapter</name>
    <message>
        <location filename="../src/app/utilsadapter.cpp" line="334"/>
        <source>%1 Mbps</source>
        <translation>%1 Mbit/s</translation>
    </message>
    <message>
        <location filename="../src/app/utilsadapter.cpp" line="334"/>
        <source>Default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../src/app/utilsadapter.cpp" line="539"/>
        <source>System</source>
        <translation>System</translation>
    </message>
</context>
<context>
    <name>lrc::ContactModelPimpl</name>
    <message>
        <location filename="../src/libclient/contactmodel.cpp" line="478"/>
        <source>Searching…</source>
        <translation>Søger…</translation>
    </message>
    <message>
        <location filename="../src/libclient/contactmodel.cpp" line="1015"/>
        <source>Invalid ID</source>
        <translation>Ugyldigt ID</translation>
    </message>
    <message>
        <location filename="../src/libclient/contactmodel.cpp" line="1018"/>
        <source>Username not found</source>
        <translation>Brugernavn ikke fundet</translation>
    </message>
    <message>
        <location filename="../src/libclient/contactmodel.cpp" line="1021"/>
        <source>Couldn&apos;t lookup…</source>
        <translation>Jeg kunne ikke se efter...</translation>
    </message>
</context>
<context>
    <name>lrc::api::ContactModel</name>
    <message>
        <location filename="../src/libclient/contactmodel.cpp" line="441"/>
        <source>Bad URI scheme</source>
        <translation>Ugyldigt URI-skema</translation>
    </message>
</context>
</TS>